---
title: Toetsing privacy- en informatiebeveiliging
weight: 4
---
Aanbieders verwerken persoonsgegevens bij de totstandkoming van de aanbiedergegevensset en de gegevenssets voor specifieke uitvragen. Aangezien wettelijke grondslagen geen of nauwelijks ruimte laten voor de verstrekking van deze persoonsgegevens aan afnemers, is het van belang privacy en informatiebeveiliging goed te regelen en blijvend te toetsen.

Hiervoor gelden de volgende afspraken:

- Bij het opstellen en wijzigen van uitwisselprofielen wordt door de beheerorganisatie steeds getoetst in hoeverre aanbieders met de voorgestelde aggregatie kunnen komen tot onomkeerbare anonimisering;
- Bij het opstellen en wijzigen van uitwisselprofielen wordt door de beheerorganisatie steeds getoetst of een afnemer door de combinatie van gegevens uit het uitwisselprofiel met andere uitwisselprofielen tot persoonsgegevens kan komen;
- Alle uitwisselprofielen worden eens in de twee jaar door de beheerorganisatie getoetst om voortgeschreden inzichten op het gebied van anonimiseren van data te kunnen ondervangen.
  
Aanvullend dient de beheerorganisatie voor bredere toetsing op het gebied van privacy en informatiebeveiliging te zorgen bij significante wijzigingen aan de afsprakenset en/of de gebruikte techniek (denk aan de invoering van gegevensuitwisseling met datastations).