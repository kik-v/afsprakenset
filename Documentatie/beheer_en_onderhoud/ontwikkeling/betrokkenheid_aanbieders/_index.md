---
title: Betrokkenheid aanbieders bij ontwikkeling
weight: 2
---
## Doel van betrokkenheid aanbieders in de ontwikkeling van de afsprakenset

* Toets op de gegevens (juiste definitie) in (of voor opname in) de modelgegevensset.
* Toets op toepasselijkheid modelgegevensset in verschillende situaties bij aanbieders (o.a. afhankelijk van inrichting informatievoorziening en volwassenheidsniveau datagericht werken).
* (mede) Bepalen belang en nut/noodzaak van een onderdeel in het modeluitwisselprofiel.
* Input voor de uitwisselkalender en mede bewaken van de uitwisselkalender.

## Doel van betrokkenheid aanbieders in de ontwikkeling van de uitwisselprofielen

* Toets op achtergrond en onderbouwing betreffende uitvraag en bijbehorende informatievragen. Adviesrol richting de afnemer over nut- en noodzaak van informatievragen.
* Adviesrol richting afnemer en beheerorganisatie in het matchingsproces op de hierin benoemde stappen om te komen tot een gedragen uitwisselprofiel.
* Adviesrol richting afnemer en beheerorganisatie bij de (verdere) invulling van en te maken keuzes in betreffende uitwisselprofiel (op basis van de benoemde onderwerpen in het modeluitwisselprofiel) evenals een toets op conceptvoorstellen voor dat uitwisselprofiel.
* (mede) Bepalen belang en nut/noodzaak peilmoment, peilperiode en uitvraagmoment in relatie tot de uitwisselkalender.
* Toets op algemene praktische uitvoerbaarheid en implementeerbaarheid van de voorgenomen afspraken inclusief de benodigde doorlooptijd voor implementatie.
* Advies beheerorganisatie op logische samenhang tussen de bestaande uitwisselprofielen

## Vorm van betrokkenheid

* Zorgaanbieders kunnen individueel hun inbreng leveren via de reguliere kanalen en bijbehorende processen, zoals ingericht door de beheerorganisatie. Het gaat dan bijvoorbeeld om het inbrengen van een wijzigingsverzoek of deelname aan praktijktoetsen.
* Actiz kan namens zorgaanbieders inbreng leveren via de reguliere kanalen en bijbehorende processen. Daarnaast kan Actiz via de governancestructuur van KIK-V het geluid van de achterban inbrengen of producten beoordelen.
* In het ontwikkelproces van de afsprakenset en uitwisselprofiel wordt gezamenlijk met Actiz bepaald of, hoe en met welke zorgaanbieders deze afgestemd worden. Voorbeeld: een wijzigingsverzoek op het juridisch kader of voorgestelde maatregelen op het vlak van privacy kan belegd worden in een ad hoc georganiseerde juridische werkgroep. De beheerorganisatie stemt met ActiZ af voor welke verzoeken inbreng van zorgaanbieders (en/of andere branches) nodig is en op welke manier dit georganiseerd moet worden (en met wie).
* ActiZ kan ervoor kiezen voorafgaand aan de besluitvorming in de Ketenraad nog een bredere consultatie te doen bij de achterban (en/of andere brancheorganisaties) ten aanzien van de voorstellen. Zij is op de hoogte van de achtergrond bij de voorstellen via de deelname aan de werkgroep.
* Voor het toetsen van de (nieuwe) uitwisselprofielen wordt in samenspraak met Actiz een aantal zorgaanbieders benaderd en betrokken. In een bredere klankbordgroep sluiten extra zorgaanbieders aan, evenals Actiz zelf, om aandachtspunten uit de praktijktoetsen gezamenlijk te bespreken en beoordelen.
* Voor wijzigingsverzoeken op een bestaand uitwisselprofiel beoordeelt de beheerorganisatie in samenspraak met de afnemer en ActiZ of inbreng van zorgaanbieders nodig is en op welke manier dit georganiseerd moet worden (en met wie).