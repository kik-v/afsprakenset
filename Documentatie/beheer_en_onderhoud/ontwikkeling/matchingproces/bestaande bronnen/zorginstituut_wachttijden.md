---
title: LRZA WZD en WVGGZ registratie
weight: 11
---

In opdracht van het ministerie van VWS publiceert Zorginstituut Nederland maandelijks (rond het einde van de navolgende maand) gedetailleerde overzichten van het aantal mensen dat wacht op langdurige zorg.

Er is wachtlijstinformatie op landelijk ([Wachtlijsten landelijk niveau | Zorgcijfersdatabank.nl](https://www.zorgcijfersdatabank.nl/toelichting/wachtlijstinformatie/wachtlijsten-landelijk-niveau)) en op instellingsniveau ([Wachtlijsten instellingsniveau | Zorgcijfersdatabank.nl](https://www.zorgcijfersdatabank.nl/toelichting/wachtlijstinformatie/wachtlijsten-instellingsniveau)). Op de pagina ‘Landelijk niveau’ staan de pdf's met overzichten van landelijke wachtlijstinformatie, op de pagina 'Instellingsniveau' de pdf's met wachtlijstrapportage per sector. 

Alle zorgkantoren in Nederland leveren maandelijks bij Zorginstituut Nederland de gegevens aan van het aantal wachtenden op langdurige zorg. Deze gegevens zijn afkomstig uit de zogeheten AW317-wachtlijstbestanden van de zorgkantoren. Zo kunnen elke maand gedetailleerde overzichten met wachtlijstinformatie worden samengesteld. Om van wachtlijstbestanden tot wachtlijstinformatie te komen, voert het Zorginstituut een zorgvuldige berekening uit. De bestandsgegevens worden gefilterd, gesorteerd en gecalculeerd volgens de Rekenregels wachtlijsten. De rekenregels staan hier: [povoopen-2015069433-v12l-rekenregels_wachtlijsten.docx](https://view.officeapps.live.com/op/view.aspx?src=https%3A%2F%2Fwww.zorgcijfersdatabank.nl%2Fbinaries%2Fcontent%2Fassets%2Fzorgcijfersdatabank%2Fwachtlijstinformatie-wlz%2Fpovoopen-2015069433-v12l-rekenregels_wachtlijsten.docx&wdOrigin=BROWSELINK)

## Welke informatie toont de wachtlijstinformatie?
Vanaf januari 2021 zijn er in de langdurige zorg 4 nieuwe wachtstatussen. Elke status kent meerdere classificaties die inzicht geven in de wens of behoefte van de cliënt. De classificatie geeft de situatie van de cliënt aan en zegt ook iets over de urgentie. De 4 wachtstatussen zijn:

* Urgent plaatsen: wachtstatus die aangeeft dat opname voor de cliënt op (zeer) korte termijn noodzakelijk is (opnamenoodzaak);
* Actief plaatsen: wachtstatus die aangeeft dat opname voor de cliënt binnen 1 of enkele maanden noodzakelijk is (opnamebehoefte);
* Wacht op voorkeur: wachtstatus die aangeeft dat de cliënt opgenomen wil worden, maar pas als aan een aantal randvoorwaarden wordt voldaan (opnamewens);
* Wacht uit voorzorg: wachtstatus die aangeeft dat de cliënt zich in de thuissituatie prima kan redden, maar wel op een wachtlijst wil staan bij een voorkeuraanbieder (geen opnamewens).

