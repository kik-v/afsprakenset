---
title: Bestaande (openbare) bronnen
weight: 1
---
Onderstaande (openbare) databronnen hebben betrekking op verantwoording van kwaliteit en bedrijfsvoering in de zorg. Deze bronnen bevatten mogelijk al informatie die niet opnieuw uitgevraagd hoeft te worden bij zorgaanbieders via een uitwisselprofiel. Om te bepalen of deze informatie toegepast kan worden door een afnemer, volgt in de afsprakenset meer informatie over de inhoud van deze bronnen. Het gaat hierbij om de volgende bronnen:
* CBS 
* LRZA (CIBG)
* Databank CIZ 
* Inspectierapporten IGJ  
* Pensioenfonds Zorg en Welzijn 
* Regionanalyses NZA 
* Zorgcijfersdatabank 
* Zorgkaart Nederland 

## Toetsingscriteria (openbare) bronnen
Een databron kan worden toegevoegd aan de set van afspraken wanneer deze als van toegevoegde waarde wordt beoordeeld door de beheerorganisatie. De beoordeling vindt plaats aan de hand van een afweging van de toegevoegde waarde enerzijds, en onderstaande randvoorwaardelijke uitgangspunten anderzijds. De uitgangspunten zijn een indicatie of een informatiebron van waarde is, de uiteindelijke afweging is hierin doorslaggevend voor de opname van de informatiebron in de afsprakenset. (Openbare) bronnen van een ketenpartij KIK-V die worden gevoed met behulp van een uitwisselprofiel KIK-V worden niet in deze lijst opgenomen. Dit zou tenslotte een ‘cirkelredenering’ opleveren. Deze bronnen worden gevoed met behulp van het uitwisselprofiel KIK-V, wat betekent dat vragen beantwoord kunnen worden met behulp van de model gegevensset KIK-V. Een andere ketenpartij kan op basis van diezelfde gegevensset eigen vragen stellen in het uitwisselprofiel en hoeft hiervoor niet gebruik te maken van aangeleverde antwoorden bij een andere ketenpartij KIK-V. 

### Randvoorwaardelijke uitgangspunten
1. De informatiebron is reeds in gebruik door één of meerdere partijen binnen het programma KIK-V;
2. De informatiebron is valide: in de informatiebron staat de methode omtrent informatieverzameling beschreven;
3. De informatiebron is betrouwbaar;
4. De informatiebron is actueel: de informatiebron bevat actuele gegevens en zijn relevant voor het kader waarin de informatie uit wordt ontsloten;
5. De informatiebron is volledig: de informatiebron is representatief voor datgene dat de informatiebron presenteert;
6. De informatiebron wordt beheerd: de informatiebron moet door een persoon of organisatie worden beheerd.

## Overzicht

* [Centraal bureau voor de statistiek (CBS)](/content/docs/beheer_en_onderhoud/ontwikkeling/matchingproces/bestaande%20bronnen/cbs.md)
* [LRZA (CIBG)](/content/docs/beheer_en_onderhoud/ontwikkeling/matchingproces/bestaande%20bronnen/cibg.md)
* [Databank CIZ](/content/docs/beheer_en_onderhoud/ontwikkeling/matchingproces/bestaande%20bronnen/databank_CIZ.md)
* [Inspectierapporten IGJ](/content/docs/beheer_en_onderhoud/ontwikkeling/matchingproces/bestaande%20bronnen/inspectierapporten_IGJ.md)
* [Pensioenfonds Zorg en Welzijn (PFZW)](/content/docs/beheer_en_onderhoud/ontwikkeling/matchingproces/bestaande%20bronnen/pfzw.md)
* [Regioanalyses Nederlandse Zorgautoriteit](/content/docs/beheer_en_onderhoud/ontwikkeling/matchingproces/bestaande%20bronnen/regio_analyses_nza.md)
* [Zorgcijfersdatabank](/content/docs/beheer_en_onderhoud/ontwikkeling/matchingproces/bestaande%20bronnen/zorgcijfersdatabank.md)
* [Zorgkaart Nederland](/content/docs/beheer_en_onderhoud/ontwikkeling/matchingproces/bestaande%20bronnen/zorgkaart_nederland.md)