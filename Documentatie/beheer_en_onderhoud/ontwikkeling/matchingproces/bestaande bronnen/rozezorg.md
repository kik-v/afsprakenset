---
title: Roze zorg certificaat - rozezorg.nl
weight: 9
---

## Roze Loper certificaat

rozezorg.nl is een website van de St. Roze 50+ en biedt een overzicht van alle Roze Loper organisaties. De Roze Loper is het keurmerk voor lhbti-acceptatie en inclusiviteit, Roze50+ is eigenaar van dit keurmerk. De Roze Loper wordt uitgereikt na een traject dat leidt naar sociale acceptatie van seksuele diversiteit in de zorg- gevolgd door een audit door een onafhankelijke Certificerende Instelling. Cliënten voelen zich bij een Roze Loper organisatie meer gezien, voelen zich veilig en geaccepteerd in hun seksuele en genderidentiteit. Daarnaast wordt de Roze Loper organisatie zelf meer inclusief naar minderheidsgroepen in het algemeen. De Roze Loper kan gebruikt worden in woonzorg- en thuiszorginstellingen, in organisaties voor mensen met een beperking, welzijnsorganisaties, GGZ en ziekenhuizen.

Op de website is een overzicht te vinden van instellingen die het keurmerk mogen gebruiken: https://rozezorg.nl/organisaties 

