---
title: Databank CIZ
weight: 4
---
De Databank CIZ biedt onderzoekers, gemeenten en zorgaanbieders inzicht in het aantal cliënten dat in Nederland recht heeft op zorg uit de Wlz.

De beschikbare gegevens toont het aantal cliënten met regulier Wlz-recht. Daarnaast toont het ook het aantal cliënten dat gebruikt maakt van de subsidieregelingen, het aantal dat valt onder de groep Wlz-indiceerbaren of waarbij een partnerverblijf aan de orde is.

De landelijke cijfers zijn onder meer uit te splitsen naar landsdeel, provincie en gemeente. Ook zijn de resultaten te verfijnen tot specifieke grondslagen, zorgprofielen en leeftijdscategorieën. De Databank toont data vanaf 2015.
Zie hier voor meer informatie:

* [https://www.ciz.nl/nieuws/nieuwe-gegevens-in-databank-ciz](https://www.ciz.nl/nieuws/nieuwe-gegevens-in-databank-ciz)
* [https://ciz.databank.nl/jive](https://ciz.databank.nl/jive)