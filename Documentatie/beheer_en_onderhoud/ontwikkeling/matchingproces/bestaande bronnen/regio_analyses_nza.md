---
title: Regioanalyses Nederlandse Zorgautoriteit
weight: 8
---
De NZa heeft regioanalyses opgesteld waarmee zij regionale zorgpartijen handvatten wil bieden bij het in kaart brengen van kansen om de gezondheid in de regio te verbeteren en knelpunten weg te nemen. De NZa-analyse biedt inzicht in de populatie, het zorggebruik en de toegang tot zorg in de regio, ten opzichte van het landelijk beeld (de hele populatie). Voor iedere regio is de analyse op dezelfde manier uitgevoerd waardoor inzichtelijk wordt waarin een regio zich onderscheidt van andere regio’s. Deze NZa-regioanalyse is een eerste bijdrage aan méér inzicht in de verschillende opgaves waar de regio’s voor staan. De partijen die in de regio’s werken aan regiobeelden en –plannen, moeten deze verrijken met specifieke kenmerken van de regio en aanvullende informatie over vraag naar en aanbod van zorg. Nog veel belangrijker is dat zij de inzichten gebruiken om hun acties en de samenwerking in de regio voort te zetten en waar nodig intensiveren en uitbreiden.

Zie voor meer informatie: [Overzichtspagina regioanalyses Nederlandse Zorgautoriteit - Nederlandse Zorgautoriteit](https://puc.overheid.nl/nza/doc/PUC_725254_22/1/)
