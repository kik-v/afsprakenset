---
title: LRZA WZD en WVGGZ registratie
weight: 6
---

Een zorgaanbieder die gedwongen zorg verleent onder de Wet zorg en dwang of Wet verplichte ggz moet zijn locaties geregistreerd hebben in het openbaar locatieregister. 

## Locatieregister
In het zorgaanbiedersportaal staat informatie over alle zorgaanbieders in Nederland die geregistreerd zijn in diverse zorgregisters. Hierin is ook te zien of een zorgaanbieder locaties heeft geregistreerd waar gedwongen zorg onder de Wzd of Wvggz wordt verleend. Het zorgaanbiedersportaal is te raadplegen via https://zoeken.zorgaanbiedersportaal.nl/. 

Nieuwe locaties moeten aangemeld worden door de zorgaanbieder. Dat is nodig als er gedwongen zorg in of vanuit een locatie wordt verleend die nog niet in het register staat. Zorgaanbieders gebruiken voor de registratie van een nieuwe locatie bij voorkeur het vestigingsnummer van deze locatie uit het Handelsregister.
 
Voordat de zorgaanbieder met zijn locatie(s) wordt opgenomen in het register vindt geen toets plaats van de inspectie. Uiteraard moet de instelling wel aan alle eisen van gedwongen zorg voldoen.

## Uitzondering voor forensische zorg
Rijksinstellingen voor de verlening van forensische zorg zijn van de registratieverplichting uitgezonderd. Zij hoeven niet in het register te staan.
