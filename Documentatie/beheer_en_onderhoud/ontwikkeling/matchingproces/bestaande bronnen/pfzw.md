---
title: Pensioenfonds Zorg en Welzijn (PFZW)
weight: 7
---
Verpleeghuizen leveren gegevens aan over personeel bij de Belastingdienst ten behoeve van de loonbelasting. Dezelfde gegevensstroom wordt gelijktijdig bij het Pensioenfonds Zorg&Welzijn (PFZW) aangeleverd voor het uitvoeren van haar taken.

Pensioenfonds Zorg en Welzijn (PFZW) heeft de volgende informatie:

- CAO
- Gegevens m.b.t. arbeidsovereenkomsten: onbepaalde tijd, oproepovereenkomst, functieomschrijving, contractomvang, parttime percentage
- Aantal verloonde uren
- Aantal gewerkte dagen
- Gegevens m.b.t. verlof: soort verlof, uren verlof, percentage verlof

Meer informatie over de gegevens die worden uitgewisseld met het pensioenfonds is op te maken aan uit de documentatie over de standaard voor aanlevering: https://www.sivi.org/standaarden/uniforme-pensioenaangifte/

Het betreft hier geen openbare bron. In overleg met PFZW kan een afnemer afspraken maken over levering van informatie. Daarbij moet met de volgende punten rekening worden gehouden:

- De gegevens hebben enkel betrekking op personeel in dienst bij de werkgever, de verpleeghuisinstelling. Gegevens over inhuur, uitzendkrachten en vrijwilligers zijn daarmee niet voorhanden. 
- PFZW is (op dit moment) niet in staat om gegevens terug te leveren aan zorginstellingen of zorginstellingen gegevens te laten valideren of accorderen. PFZW is daarmee vooral een bron voor gegevens waarbij de zorginstelling niet verantwoordelijk hoeft te blijven voor de aanlevering.
- PFZW heeft gegevens op organisatieniveau inzichtelijk. Het is niet mogelijk om gegevens op locatieniveau te ontvangen.