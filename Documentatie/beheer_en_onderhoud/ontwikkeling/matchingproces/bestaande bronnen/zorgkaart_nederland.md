---
title: Zorgkaart Nederland
weight: 12
---
ZorgkaartNederland biedt onafhankelijke keuze-informatie om patiënten te helpen kiezen voor een zorgverlener die het beste past bij hun wensen. ZorgkaartNederland is een initiatief van [Patiëntenfederatie Nederland](http://www.patientenfederatie.nl/).

ZorgkaartNederland werkt aan openheid in de zorg. De waarderingen van patiënten laten zien hoe mensen zorg ervaren. Ook biedt ZorgkaartNederland andere informatie over de kwaliteit van zorgaanbieders. Bijvoorbeeld in [keuzehulpen](https://www.zorgkaartnederland.nl/keuzehulpen). Die kun je gebruiken om te kijken welke zorgaanbieder voor jouw aandoening het beste voldoet aan jouw wensen.

Voor zorgaanbieders is ZorgkaartNederland een kwaliteitsinstrument, waarmee ze inzicht krijgen in wat volgens patiënten goed gaat en beter kan. Zorgaanbieders die een pakket hebben van ZorgkaartNederland kunnen onder andere in een dashboard geanonimiseerde waarderingen monitoren en trends analyseren.

Zie voor meer informatie over de Zorgkaart: [https://www.zorgkaartnederland.nl/](https://www.zorgkaartnederland.nl/)
