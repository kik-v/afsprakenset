---
title: LRZA (CIBG)
weight: 3
---
Het Landelijk Register Zorgaanbieders (LRZa) is het openbare register waarin alle zorgaanbieders (ondernemingen) geregistreerd staan. De wettelijke basis van het LRZa is artikel 12 van de Wkkgz.

Het doel van het Landelijk Register Zorgaanbieders (LRZa) is om duidelijk te maken wie, waar, welke zorg verleent en met welke bevoegdheid. Zo heeft de burger inzicht in waar hij welke zorg kan verkrijgen.

Daarnaast maken ook diverse (overheids)organisaties gebruik van het LRZa voor bijvoorbeeld beleidsvorming of toezicht. Deze landelijke en openbare registratie van zorgaanbieders zorgt voor goed toezicht en een efficiënte uitvoering binnen de zorg.

## Gegevens uit vijf bronnen
Het LRZa registreert zelf niet direct informatie maar baseert zich op een aantal onderliggende bronnen en (wettelijke) basisregistraties. Hierbij wordt maximaal gebruik gemaakt van de gegevens die door zorgondernemingen al bij andere instanties zijn gemeld om (onnodige) dubbele registratie en daarmee gepaard gaande extra lastendruk te voorkomen.

Het LRZA brengt informatie uit diverse relevante basisregistraties in samenhang: het Handelsregister van de Kamer van Koophandel, het AGB-register (Algemeen Gegevens Beheer Zorgverleners) van Vektis en het BIG-register. Daarnaast ontsluit het LRZa ook informatie uit het openbaar databestand kwaliteitgegevens zorg van Zorginstituut Nederland en de Jaarverantwoording Zorg.  
Verder zijn er partijen die geen gegevens leveren maar wel gebruik maken van het LRZa zoals de Nederlandse Zorgautoriteit en de Inspectie Gezondheidszorg en Jeugd.

## Hoe wordt een zorgaanbieder geregistreerd in het LRZa ?
Momenteel wordt een zorgaanbieder automatisch toegevoegd aan het LRZa wanneer die zich registreert in het Handelsregister met een SBI-code uit een afgebakende set SBI-codes in het domein Zorg en Welzijn*. Indien een zorgonderneming geen zorg SBI-code heeft maar wel een AGB-code krijgt, dan wordt deze ook toegevoegd aan het LRZa (een voorbeeld is een taxionderneming die ziekenvervoer gaat verzorgen).

Met de komst van de [Wtza](https://www.toetredingzorgaanbieders.nl/) worden zorgaanbieders ook toegevoegd wanneer ze voldoen aan de meldplicht en/of een toelatingsvergunning krijgen. In de meeste gevallen zullen deze reeds in het LRZa staan omdat ze een zorg SBI-code hebben. Vanaf 1 januari 2022 krijgen zorgaanbieders pas een AGB-code als ze zich gemeld hebben.

Zie voor meer informatie: [https://www.cibg.nl/lrza](https://www.cibg.nl/lrza)