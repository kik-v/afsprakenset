---
title: Commissie Expertisecentra Langdurige zorg - celz.nl
weight: 2
---

Op https://www.commissiecelz.nl/doelgroepnetwerken staan vermeld welke doelgroepnetwerken er zijn ingericht voor de behandeling van ziektebeelden van langdurig zieke cliënten die ook wel ‘laag volume, hoog complex’ (lvhc-)doelgroepen worden genoemd. Deze doelgroepnetwerken bestaan uit zorgaanbieders die gespecialiseerd zijn in die doelgroepen en in samenwerking de juiste zorg kunnen leveren. 

Per doelgroep is een tabel opgenomen met instellingen die deel uitmaken van het netwerk. Bijvoorbeeld voor de doelgroep Korsakov. 
