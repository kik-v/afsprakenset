---
title: Beheer en onderhoud
weight: 11
---
## Inleiding
Om efficiënt en effectief beheer en onderhoud over de KIK-V producten uit te kunnen voeren is de inrichting van de onderliggende beheerprocessen noodzakelijk. Deze beheerprocessen borgen de continuïteit van de in beheer genomen producten. De Information Technology Infrastructure Library (ITIL) is de meest gebruikte standaard voor het inrichten van deze beheerprocessen, waarbij wordt uitgegaan van service support en service delivery processen.

In het kader van (door)ontwikkeling, beheer en onderhoud kent de Beheerorganisatie KIK-V een gestructureerd proces voor het voortbrengen van nieuwe release van KIK-V producten, bestaande uit meerdere fasen met bijbehorende activiteiten, taken en verantwoordelijkheden, die gezamenlijk het releaseproces vormen. Onder een release van een KIK-V product wordt het volgende verstaan:

‘_Een nieuwe versie van een KIK-V product in kader van (door)ontwikkeling en onderhoud die gepubliceerd wordt ter implementatie_.’

De activiteiten binnen dit releaseproces worden uitgevoerd in afstemming en samenwerking met alle relevante deelnemers om in te kunnen spelen op functionele, technische en operationele afhankelijkheden. Om transparantie, structuur en flexibiliteit in het releaseproces in kader van beheer en onderhoud te borgen, worden de vastgestelde afspraken rondom samenwerking en governance uit de Afsprakenset gehanteerd. Alle werkzaamheden, processtappen en afspraken tezamen leveren een gestandaardiseerd proces op voor het voortbrengen van nieuwe releases van KIK-V producten. De releases worden in fases voorbereid, uitgewerkt en opgeleverd. Deze fases van het releaseproces zijn onderdeel van het releasebeleid van de Beheerorganisatie.

De Beheerorganisatie is verantwoordelijk voor het borgen en structureren van het releaseproces van KIK-V producten, zodat:

* de KIK-V producten voldoen aan de technische en functionele vereisten;
* de KIK-V producten in lijn zijn met de juridische grondslagen;
* de vastgestelde governancestructuur wordt nageleefd.

## Doelen
De beheerafspraken kennen als onderdeel van de Afsprakenset een aantal doelen:

* **Bijdragen aan transparantie:** De beheerafspraken dienen transparantie te borgen door inzicht te geven in de processen, rollen en verantwoordelijkheden van betrokken partijen en het besluitvormingsproces rondom beheer en onderhoud van de KIK-V producten. Door de processtappen navolgbaar te maken wordt het vertrouwen in de beheerprocessen en de KIK-V producten vergroot.
* **Bijdragen aan standaardisatie:** De beheerafspraken dienen bij te dragen aan het standaardiserend effect van KIK-V. Standaardisatie is voor zowel aanbieders (o.a. minder administratieve lasten) als afnemers (o.a. betere kwaliteit van gegevens) van meerwaarde.
* **Bieden van voldoende flexibiliteit:** Het is voor afnemers niet altijd mogelijk om van tevoren te voorspellen welke informatie zij nodig hebben. De beheerprocessen zullen hier rekening mee moeten houden en een zekere mate van flexibiliteit mogelijk maken, om draagvlak voor de KIK-V producten te kunnen borgen.

## Proces

De beheerafspraken zijn opgebouwd aan de hand van de zes fasen van het releaseproces, namelijk:

1. **Verkennen**: In het kader van wensen tot doorontwikkeling of feedback op huidige KIK-V producten kunnen er zowel intern als extern wijzigingsverzoeken worden aangedragen.  De fase Verkennen beschrijft het proces rondom het inventariseren van ontwikkelingen en wijzigingsverzoeken en het bepalen van de noodzakelijkheid en impact van de desbetreffende wijziging.
2. **Concretiseren**: Indien tijdens de Fase Verkennen is besloten om met een wijzigingsverzoek aan de slag te gaan, dient er een ontwikkelproces met bijbehorende activiteiten en planning in werking te worden gesteld. De fase Concretiseren beschrijft hoe dit ontwikkelproces vorm krijgt.
3. **Vaststellen**: Nadat alle ontwikkelactiviteiten voor het voortbrengen van de nieuwe release van een KIK-V product zijn afgerond en de nieuwe release opgeleverd kan worden voor publicatie, dient de nieuwe release te worden vastgesteld. De fase Vaststellen beschrijft de relevante afspraken over het vaststellen van nieuwe releases van KIK-V producten.
4. **Publiceren**: Nieuwe versies van de afsprakenset en/of uitwisselprofielen dienen na definitieve vaststelling te worden gepubliceerd, alvorens ze kunnen worden geïmplementeerd. De fase Publiceren beschrijft de afspraken die daarop van toepassing zijn.
5. **Implementeren**: Nieuwe versies van KIK-V producten dienen na definitieve vaststelling te worden gepubliceerd, alvorens ze kunnen worden geïmplementeerd. Implementeren beschrijft de afspraken en processen die daarvoor gelden.
6. **Evalueren**: De implementatie van de nieuwe releases van KIK-V producten zullen bij gebruik in de praktijk worden toegepast en nieuwe wensen opleveren voor doorontwikkeling. De fase Evalueren beschrijft de afspraken rondom het benaderen van ontvangen feedback en ontwikkelwensen. Deze zijn tevens van toepassing op de beheerafspraken zelf.

De afbeelding hieronder (_Figuur 1_) geeft een schematische weergave van de zes fasen van het releaseproces voor het voortbrengen van nieuwe versies van KIK-V producten n.a.v. wijzigingsverzoeken en wensen tot doorontwikkeling.

![Releaseproces](releaseproces.png)

**_Figuur 1._** _Schematische weergave van de zes fasen van het generieke releaseproces voor KIK-V producten._

## Aandachtpunten doorlooptijd
De doorlooptijd van het releaseproces is afhankelijk van de aard en omvang van de voort te brengen release, de impact van de nieuwe release op andere KIK-V producten en de benodigde besluitvorming rondom vaststelling en publicatie volgens de geldende afspraken van KIK-V. 

Zo is de ontwikkeling van uitwisselprofielen een proces tussen afnemer en uitvoerend orgaan en kent een “lichter” en flexibeler karakter dan de ontwikkeling van de Afsprakenset. Zolang een uitwisselprofiel aansluit bij de Afsprakenset, kan de ontwikkeling van het uitwisselprofiel hierdoor “lean and mean” blijven. De ontwikkeling van de Afsprakenset is een proces tussen alle belanghebbenden en kent een voorspelbaarder karakter met een vaste ontwikkelcyclus. De afspraken die gelden voor de twee verschillende ontwikkelcycli staan nader beschreven bij Ontwikkeling uitwisselprofielen en Ontwikkeling Afsprakenset.

Dit verschil in de zwaarte van de ontwikkelingscycli vertaalt zich naar verschillen in doorlooptijd voor het doorvoeren van een wijziging. Een wijziging op een bestaand uitwisselprofiel – waarbij geen sprake is van aanpassingen aan andere KIK-V producten – is beduidend korter vergeleken met het voortbrengen van een nieuwe versie van de Afsprakenset, waarbij de doorlooptijd langer is gezien het bijkomende besluitvormingsproces met vaststelling via de Ketenraad en de samenhang tussen de Afsprakenset en de andere KIK-V producten; een wijziging aan de Afsprakenset is van invloed op de op dat moment geldende versies van andere KIK-V producten. Klik hier voor meer informatie over het ontwikkelings- en besluitvormingsproces  van de Afsprakenset en Uitwisselprofielen. Dit verschil in doorlooptijd is tevens weergegeven in onderstaande afbeelding, waarbij de bovenste loop een korte doorlooptijd kent dan de onderste loop (Figuur 2).

![Beheerafspraken](beheerafspraken.png)

**_Figuur 2._** _Overzicht van het generieke releaseproces voor KIK-V producten met weergave van de verschillende doorlooptijden._

## 1. Verkennen

Zowel intern als extern kunnen er wensen tot doorontwikkeling en wijzigingsverzoeken worden aangedragen op de verschillende KIK-V producten. Signalen over deze wensen en wijzigingsverzoeken komen binnen bij en/of worden gedeeld met de Beheerorganisatie. Er kan onder andere sprake zijn van de volgende situaties:

* Een afnemer heeft een nieuwe of gewijzigde informatiebehoefte;
* Een deelnemer komt met een verzoek tot het wijzigen van een van de afspraken;
* Een externe standaard, die in de Afsprakenset wordt toegepast, gaat wijzigen/is gewijzigd.
* De Beheerorganisatie draagt een wens tot doorontwikkeling aan in het kader van technische of inhoudelijke ontwikkelingen.

De fase Verkennen beschrijft het proces rondom het inventariseren en verkennen van ontwikkelingen en wijzigingsverzoeken en het bepalen van de noodzakelijkheid en impact van de wijziging.

In de fase Verkennen wordt bepaald of de ontvangen wijzigingsverzoeken noodzakelijk zijn. Hierbij wordt verkend wat de impact van het wijzigingsverzoek is op zowel het te wijzigen product als op de andere KIK-V producten. In deze fase dient dus rekening gehouden te worden met de onderlinge samenhang tussen de KIK-V producten. Daarnaast wordt gekeken naar de urgentie van de wijziging en in hoeverre de wijziging past binnen de huidige uitwisselkalender. Tot slot dient er gecontroleerd te worden of de wijziging rijmt met de geldende grondslagen.

Het kan zijn dat hieruit geconcludeerd wordt dat er onvoldoende doorlooptijd is om de uitvraag uit te werken volgens KIK-V principes vanwege bepaalde urgentie of dat onvoldoende duidelijk is of dit een structurele uitvraag betreft en de uitvraag niet gemakkelijk met behulp van de geldende release van de Afsprakenset beantwoord kan worden. In dat geval treedt de [procedure Afwijkende uitvraag](https://kik-v-publicatieplatform.nl/afsprakenset/_/content/docs/beheer_en_onderhoud/afwijkende_uitvraag) in werking. Indien in samenhang met alle deelnemers wordt besloten het wijzigingsverzoek niet verder op te pakken, dan stopt het proces hier. Bij een positief besluit wordt er overgegaan tot de volgende fase. Hieronder volgt een overzicht van de doelen en kernvragen van de fase Verkennen.

### 1.1. Doel verkennen:
- Inzicht verkrijgen in wijzigingsverzoeken of ontwikkeling van nieuwe KIK-V producten;
- Bepalen wat de noodzaak en wenselijkheid is van de wijziging;
- Bepalen welke impact de wijziging met zich meebrengt;
- Controleren of de wijziging in lijn is met de desbetreffende grondslagen.


### 1.2. Relevante vragen voor Verkennen:
- Waarom dient er over te worden gegaan tot een nieuwe release en in hoeverre is de wijziging noodzakelijk?
- Wat zijn de specificaties van de nieuwe release? Wat houdt de nieuwe release in?
- Wat is de impact van de wijziging op het bestaande product en de andere KIK-V producten?
- Wordt er bij het doorvoeren van de wijziging voldaan aan de onderliggende grondslagen?
- Wat is de urgentie waarmee de wijziging moet worden opgepakt?
- Welke stappen dienen er nog gezet te worden om het ontwikkelproces van de nieuwe release in werking te stellen?
- Hoe past de wijziging in de huidige uitwisselkalender?

## 2. Concretiseren
Nadat de fase Verkennen is afgerond en het wijzigingsverzoek is ingewilligd, dient het ontwikkelingstraject te worden opgestart. In dit ontwikkelingstraject wordt het wijzigingsverzoek geconcretiseerd door het inrichten en uitvoeren van de benodigde ontwikkelactiviteiten, het opstellen van een concrete planning inclusief opleveringsdeadlines en bijbehorende deliverables en het bijhouden van een changelog van de doorgevoerde wijzigingen t.o.v. de eerdere versie of een geheel nieuw (deel) product. Meer informatie over de ontwikkeling van de [Afsprakenset](/content/docs/beheer_en_onderhoud/ontwikkeling) en de [uitwisselprofielen](/content/docs/beheer_en_onderhoud/ontwikkeling/uitwisselprofielen/) in het bijzonder kan teruggevonden worden in de Afsprakenset. 

In het kader van het waarborgen van de onderlinge samenhang tussen de KIK-V producten moeten er met de belanghebbende partijen  afspraken gemaakt worden over het publiceren, in werking treden en implementeren van nieuwe versies van de KIK-V producten, alvorens wordt overgegaan tot de fase Vaststellen. Zodoende wordt voorkomen dat er kritieke discrepanties ontstaan tussen de verschillende KIK-V producten. Enkel op het moment dat alle ontwikkelactiviteiten zijn afgerond en de nieuwe release gereed is voor publicatie, zal er worden overgegaan naar de volgende fase Vaststellen.

Hieronder volgt een overzicht van de doelen en kernvragen van de fase Concretiseren.

### 2.1. Doel Concretiseren:
- Het concretiseren van de benodigde ontwikkelingsactiviteiten voor het voortbrengen van de nieuwe release van het bestaande KIK-V product.
- Het borgen van het opstellen van de planning van de ontwikkelactiviteiten - inclusief opleveringsdeadlines met bijbehorende deliverables.
- Het uitvoeren van de ontwikkelactiviteiten voor de nieuwe release.
- Het inzichtelijk maken van de voortgang van het voorbrengen van een nieuwe release.

### 2.2. Relevante vragen voor Concretiseren:
- Wat zijn de benodigde ontwikkelactiviteiten voor het voortbrengen van de nieuwe release?
- Hoe ziet de planning voor de nieuwe release eruit?
- Via welke weg en hoe vaak vindt er afstemming plaats met alle stakeholders over de voortgang?
- Wat zijn de (tussentijdse) deadlines binnen het ontwikkeltraject voor de nieuwe release?
- Zijn de gestelde deadlines nog steeds haalbaar gezien de releaseplanning en opleverdatum?
- Zijn alle ontwikkelactiviteiten (inclusief taal- en spellingscontrole) afgerond?
  
## 3. Vaststellen
Nadat alle ontwikkelactiviteiten zijn afgerond dienen de nieuwe releases eerst te worden vastgesteld, voordat overgegaan kan worden tot publicatie. Voor het vaststellen van de nieuwe releases gelden de governance- en besluitvormingsafspraken, zoals geformuleerd staan in de [Afsprakenset](/content/docs/sturingsstructuur/).

Gezien de samenhang tussen de KIK-V producten en de impact hiervan op het vaststellingsproces, zijn de volgende principes opgesteld:

* De Ketenraad KIK-V is verantwoordelijk voor zowel het vaststellen van de Afsprakenset als de uitwisselprofielen en is verantwoordelijk voor de samenhang tussen de KIK-V producten.
* De Ketenraad KIK-V gaat bij het vaststellen van de afsprakenset na in hoeverre een gedegen proces is doorlopen en er bij de verschillende belanghebbenden draagvlak is voor de gemaakte afspraken. Bij het vaststellen van (wijzigingen aan) de KIK-V producten wordt altijd de impact op samenhangende producten meegenomen, inclusief een voorstel voor de implementatie van de benodigde wijzigingen. Op die manier wordt de samenhang geborgd tussen de KIK-V producten.
* Het vaststellen van (wijzigingen aan) uitwisselprofielen kan zelfstandig plaatsvinden, zolang het betreffende product aansluit bij de Afsprakenset. De Ketenraad KIK-V dient er bij het vaststellen op toe te zien dat dit inderdaad het geval is. Een KIK-V product dat niet aansluit bij de Afsprakenset kan pas worden vastgesteld, zodra de benodigde wijzigingen aan de Afsprakenset door de Ketenraad KIK-V zijn vastgesteld.
* Om de impact op de bestuurlijke besluitvorming te beperken, kan het Tactisch Overleg KIK-V onder mandaat van de Ketenraad KIK-V besluiten over kleine en/of bij spoedwijzigingen. Het Tactisch Overleg KIK-V legt hierover verantwoording af aan de Ketenraad KIK-V. Bij deze besluitvorming dient rekening te worden gehouden met de hiervoor beschreven aandachtspunten over samenhang.
* Bij de besluitvorming hebben de Ketenraad KIK-V en het Tactisch Overleg KIK-V een informatieachterstand ten opzichte van de Beheerorganisatie. Om goed te kunnen besluiten en te bepalen in hoeverre er bij de besluitvorming risico’s worden gelopen, is het van belang dat de Beheerorganisatie nieuwe releases voorziet van een advies. Het advies dient eventuele risico’s zichtbaar te maken door aan te geven in hoeverre er in de voorliggende release “Requests for comments” (RFC) zitten waarbij:
  * perspectieven van belanghebbende partijen sterk uiteen liepen, in hoeverre vervolgens tot consensus is gekomen en zo niet, welke positionering de Beheerorganisatie voorstelt. Denk bijvoorbeeld aan RFC’s voor de Modelgegevensset waarbij de belangen van aanbieders en afnemers uiteenlopen. Voor het voorgaande voorbeeld geldt dat het [Matchingsproces](/content/docs/beheer_en_onderhoud/ontwikkeling/matchingproces/) de relevante afwegingen beschrijft. De Ketenraad en het Tactisch Overleg zullen met name op de hoogte moeten zijn van die afwegingen waarbij perspectieven uiteenlopen (Vb: het al dan niet hergebruiken van een bestaande bron, het al dan niet toevoegen van gegevens aan de Modelgegevensset, etc.).
  * belangrijke belanghebbende partijen (om wat voor reden dan ook) niet betrokken zijn geweest;
  * is afgeweken van het ontwikkelproces zoals beschreven in de Beheerafspraken (Vb: het beperkt organiseren van toetsing door de mate van urgentie van een RFC).

Zodra de nieuwe releases definitief volgens bovengenoemde afspraken zijn vastgesteld, kunnen de nieuwe releases worden aangeleverd ter publicatie. De Beheerorganisatie doorloopt als laatste controle bij aanlevering de acceptatiecriteria voor het desbetreffende KIK-V product, om te bepalen of de aangeleverde release gereed is voor publicatie.

Hieronder volgt een overzicht van de doelen en kernvragen van de fase Vaststellen.

### 3.1. Doel Vaststellen:
- Het verifiëren of alle ontwikkelingsactiviteiten zijn afgerond en of de nieuwe release klaar is voor publicatie.
- Het doorlopen van de geldende governance- en besluitvormingsafspraken voor het definitief vaststellen van de nieuwe release.
- Het doorlopen van de acceptatiecriteria van de nieuwe release door de Beheerorganisatie ter voorbereiding op publicatie.

### 3.2. Relevante vragen voor Vaststellen:
- Zijn alle ontwikkelactiviteiten afgerond?
- Kan de nieuwe release worden voorgelegd ter besluitvorming en vaststelling volgens de geldende governance- en besluitvormingsafspraken?
- Voldoen de KIK-V producten, die zijn aangeboden ter publicatie, aan de acceptatiecriteria?

## 4. Publiceren
Nadat de nieuwe releases in de vorige fase zijn vastgesteld en blijken te voldoen aan de acceptatiecriteria voor publicatie, zal de Beheerorganisatie het publicatieproces opstarten en overgaan tot publicatie. Bij het publiceren en in werking laten treden van de nieuwe releases dient rekening gehouden te worden met product-specifieke afspraken rondom publicatie en versionering.

Voor de KIK-V producten gelden de volgende publicatie-afspraken:

* Elk KIK-V product is publiek toegankelijk en hebben elk een eigen publicatieplaats.
* De KIK-V producten kunnen in de tijd los van elkaar worden gepubliceerd.
* Publicatie van KIK-V producten kan, in lijn met gemaakte implementatieafspraken, op elk moment na vaststelling.
* Bij publicatie van KIK-V producten wordt vastgelegd wat het moment van inwerkingtreding is.
* Een publicatie van de Afsprakenset kan, in lijn met gemaakte implementatieafspraken, op elk gewenst moment in werking treden.
* Een publicatie van een uitwisselprofiel kan pas in werking treden indien de daarvoor benodigde Afsprakenset reeds is gepubliceerd en in werking is getreden.
* In de regel kunnen er niet meer dan twee versies van één KIK-V product actief zijn.
* De Beheerorganisatie KIK-V biedt een overzicht van de (historische en geldende) uitwisselprofielen.

Om onderscheid te kunnen maken tussen verschillende versies van de verschillende producten kent elk KIK-V product een eigen versionering volgens navolgbare systematiek.

Hieronder volgt een overzicht van de doelen en kernvragen van de fase Publiceren.

### 4.1. Doel Publiceren:
- Definitieve publicatie van vastgestelde nieuwe release KIK-V product door de Beheersorganisatie KIK-V conform product-specifieke publicatie- en versioneringsafspraken.
- Het informeren van de interne en externe stakeholders over de publicatie.

### 4.2. Relevante vragen voor Publiceren:
- Zijn de stakeholders en/of productontwikkelaars op de hoogte van de definitieve vaststelling?
- Zijn alle relevante KIK-V producten aangeleverd? Staan ze klaar voor publicatie?
- Welke specifieke afspraken gelden er voor publicatie en versionering voor de verschillende Kik-V producten?

## 5. Implementeren
Na de definitieve publicatie zal de Beheerorganisatie een interne en externe notificatie versturen over de publicatie, zodat alle relevante stakeholders op de hoogte worden gesteld. Er zal ondersteuning geboden worden bij het implementeren van nieuwe releases door deelnemers.

De implementatie van een nieuwe versie van een KIK-V product vergt aanpassingen door de deelnemers. Het is over het algemeen niet mogelijk om deze aanpassingen bij alle partijen tegelijkertijd door te laten voeren. Aan deelnemers wordt daarom bij de publicatie van een nieuwe release ruimte geboden om de nieuwe versie te implementeren binnen een gestelde implementatietermijn. Welke termijn toereikend is, wordt vastgelegd in de implementatieafspraken.

Feedback die tijdens de implementatie van de nieuwe releases door de deelnemers wordt aangeleverd zal door de Beheerorganisatie in ontvangst worden genomen ter evaluatie. Meer over het ontvangen en verwerken van feedback, wensen en verzoeken staat omschreven in de volgende fase Evalueren. Hieronder volgt een overzicht van de doelen en kernvragen van de fase Implementeren.

### 5.1. Doel Implementeren:
- Het informeren van deelnemende partijen en relevante stakeholders over de gepubliceerde nieuwe releases.
- Het (ondersteunen bij het) implementeren van een nieuwe release van een KIK-V product.
- Het inzichtelijk maken van opgehaalde en ontvangen feedback op de nieuwe release van het KIK-V product.

### 5.2. Relevante vragen voor Implementeren:
- Heeft de Beheerorganisatie alle interne en externe stakeholders op de hoogte gesteld van de publicatie?
- Kan de Beheerorganisatie (actief/passief) ondersteuning bieden in de externe communicatie en implementatie?

## 6. Evalueren
Het in gebruik nemen van nieuwe versies van KIK-V producten in praktijk kan leiden tot nieuwe wensen voor doorontwikkeling of wijzigingsverzoeken. Deze feedback in de vorm van een ge(her)formuleerde informatiebehoefte of een wijzigingsverzoek op een gepubliceerd product, kan worden ingediend bij de Beheerorganisatie, waar alle ontvangen feedback in ontvangst wordt genomen en gebundeld. Tevens zal er actief feedback worden ingewonnen. In elk uitwisselprofiel wordt een afspraak opgenomen over het evalueren van het gebruik van het uitwisselprofiel. Deze evaluatie kan leiden tot voorgestelde wijzigingen. Over de ontvangen feedback zal terugkoppeling gegeven worden aan desbetreffende productontwikkelaar(s). Vervolgens wordt de feedback geëvalueerd en bepaald wat er met de feedback dient te gebeuren. Indien de feedback verwerkt of opgepakt kan worden zonder een wijziging aan te brengen op het KIK-V product, zal het releaseproces ten einde komen. Wanneer de feedback mogelijk leidt tot een wijziging van een bestaand gepubliceerd KIK-V product, zal het releaseproces opgestart worden, beginnend met de fase “Verkennen”, voor het oppakken van het wijzigingsverzoek. Hieronder volgt een overzicht van de doelen en kernvragen van de fase Evalueren.

### 6.1. Doel Evalueren:
- Het evalueren en verwerken van de verzamelde feedback in de vorm van wensen voor doorontwikkeling en wijzigingsverzoeken.
- Bepalen welke stappen er ondernomen moeten worden op basis van de ontvangen feedback.

### 6.2. Relevante vragen voor Evalueren:
- Heeft de Beheersorganisatie de feedback ontvangen en verwerkt?
- Kan de feedback verwerkt/opgepakt worden zonder een wijziging aan te brengen in het desbetreffende KIK-V product?
- Leidt de feedback tot een verandering in een gepubliceerd KIK-V product waardoor het releaseproces vanaf de beginfase Verkennen moet worden opgestart?