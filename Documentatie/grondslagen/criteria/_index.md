---
title: Criteria
weight: 2
---
Voor de afsprakenset bestaan criteria, bestaande uit:

* [Doelen](/content/docs/grondslagen/criteria/doelen.md)
* [Randvoorwaarden](/content/docs/grondslagen/criteria/randvoorwaarden.md)