---
title: Grondplaat
weight: 3
---
![KIK-V Grondplaat](kik-v-grondplaat.png)

De afsprakenset bestaat uit drie typen producten:

* **Beheerafspraken (één)**: de beheerafspraken zijn een verzameling verantwoordelijkheden voor de uitvoering van de beheeractiviteiten;
* **Uitwisselprofielen (één of meer)**: een uitwisselprofiel is een verzameling verantwoordelijkheden van de rollen afnemer en aanbieder, die gelden voor een set van een of meer vragen en antwoorden en moeten worden toegepast in de operationele gegevensuitwisseling. Het profiel kan betrekking hebben op een of meer lagen en aspecten uit het interoperabiliteitsmodel;
* **Modelgegevensset KIK-V (één)**: de modelgegevensset KIK-V is een verzameling definities van gegevenselementen die worden gebruikt in de uitwisseling van informatie over de kwaliteit van en de bedrijfsvoering in de verpleeghuiszorg.

De afsprakenset kent verantwoordelijkheden die worden gekoppeld aan de rollen van:

* **Afnemer** (van gegevens);
* **Aanbieder** (van gegevens): dit zijn de instellingen voor verpleeghuiszorg;
* **Beheerorganisatie**: dit betreft zowel de besluitvorming als de uitvoering.

## Toepassing van de afspraken
![KIK-V Toepassing van de afspraken](kik-v-toepassing-afspraken.png)

* De afsprakenset bevat afspraken over het vraag-antwoordspel tussen afnemers en aanbieders, gericht op het uitwisselen van kwaliteitsinformatie over verpleeghuiszorg of informatie over de bedrijfsvoering in de verpleeghuiszorg.
* Een afnemer is verantwoordelijk voor het stellen van zijn vraag:
  * Op een genormaliseerde manier: de vraag moet gesteld zijn in termen van de modelgegevensset KIK-V, zodat die vraag door de aanbieder kan worden beantwoord met zijn aanbiedergegevensset KIK-V;
  * Op een valideerbare manier: de vraag moet gekoppeld worden aan en passen binnen een uitwisselprofiel.
* Een aanbieder is verantwoordelijk voor:
  * Het valideren van de vraag, om vast te stellen dat die voldoet aan een uitwisselprofiel waaraan de aanbieder zich heeft verbonden, en daarmee is gelegitimeerd; alleen dan behoeft die beantwoord te worden. Ten behoeve van validatie zijn alle goedgekeurde uitwisselprofielen door het uitvoerend orgaan op een voor aanbieders en afnemer toegankelijke plek gepubliceerd;
  * Het beantwoorden van de vraag, waarbij hij gebruik maakt van zijn aanbiedergegevensset KIK-V: een verzameling gegevens over de eigen organisatie die voldoen aan de modelgegevensset KIK-V.
* De afnemer mag ervan uit gaan dat de aanbieder over een aanbiedergegevensset KIK-V beschikt, die voldoet aan de modelgegevensset KIK-V. Of en hoe de aanbieder dit feitelijk implementeert, wordt niet voorgeschreven.

![KIK-V Vraag antwoord spel](kik-v-vraag-antwoord-spel.png)

## Totstandkoming van de afspraken
![KIK-V Vraag antwoord spel](kik-v-totstandkoming-afspraken.png)

* De beheerorganisatie is verantwoordelijk voor het matchen van enerzijds informatiebehoefte, doel en grondslag (van de afnemers), en anderzijds de registratie in het operationeel proces (door de aanbieders), zodanig dat daaruit geoptimaliseerde uitwisselprofielen en de modelgegevensset KIK-V kunnen ontstaan.
* Het handelen van de beheerorganisatie wordt ingekaderd door beheerafspraken.

## Overige producten van de beheerorganisatie
Naast de hierboven beschreven kernproducten, beheert de beheerorganisatie ook ondersteunende producten, waaronder, maar niet beperkt tot, het Juridisch kader. Dit is een beheerproduct behorende bij de uitwisselprofielen. Bepaalt (mede) grondslag & doel evenals kaderstelling inhoud & vorm in uitwisselprofiel.