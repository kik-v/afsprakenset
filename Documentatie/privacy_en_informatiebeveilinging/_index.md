---
title: Privacy en informatiebeveiliging deelnemers
weight: 5
---
In het model Uitwisselprofiel (zie hiervoor [Model uitwisselprofiel](/Documentatie/modeluitwisselprofiel/)) is opgenomen dat elk Uitwisselprofiel moet beschrijven op welke manier wordt omgegaan met privacy- en beveiliging in de betreffende uitwisseling van vraag en antwoord tussen afnemer en zorgaanbieder. In het ontwikkelproces wordt principe 28 (zie hiervoor [Principes](/Documentatie/modeluitwisselprofiel/principes.md/)) gevolgd: Antwoorden op gevalideerde vragen zijn zo goed mogelijk geanonimiseerd en bevatten geen tot de persoon herleidbare gegevens. Ook als er wel een wettelijke grondslag is voor het verwerken van persoonsgegevens door de afnemer. Indien geanonimiseerd antwoord niet mogelijk is, worden andere maatregelen getroffen ten aanzien van privacy en beveiliging.
Een zorgaanbieder verwerkt persoonsgegevens in het beantwoorden van vragen van ketenpartijen KIK-V. Artikel 7 van de Wet kwaliteit, klachten en geschillen zorg (Wkkgz) biedt een grondslag voor de opname van persoonsgegevens in de aanbiedergegevensset en de daaropvolgende aggregatie bij het beantwoorden van vragen van ketenpartijen. Zie voor meer toelichting hierop het [Juridisch kader](/Documentatie/juridisch-kader/). In principe zijn de vragen dus zo opgesteld dat de uiteindelijke antwoorden geen tot de persoon herleidbare gegevens bevatten (zie principe hierboven). 

In de Afsprakenset KIK-V zijn controles opgenomen die bij het opstellen van het Uitwisselprofiel ([Ontwikkeling uitwisselprofiel](/Documentatie/beheer_en_onderhoud/ontwikkeling/uitwisselprofielen/_index.md/)) en voorafgaand aan de vaststelling daarvan worden uitgevoerd om hierop te toetsen. De beheerorganisatie toetst doorlopend de risico’s op het gebied van privacy en informatiebeveiliging met betrekking tot de toepassing van de uitwisselprofielen en de bredere afsprakenset (zie hiervoor [Toetsing privacy- en informatiebeveiliging](/Documentatie/beheer_en_onderhoud/privacy_en_informatiebeveiliging.md)). Daarnaast zijn er procesafspraken (zie hiervoor [Onvoldoende anonimsatie](/Documentatie/beheer_en_onderhoud/onvoldoende_anonimisatie.md)) gedefinieerd voor het geval een partij toch concludeert dat sprake is van onvoldoende geanonimiseerde gegevens. 

Afnemers en aanbieders zijn zelf verantwoordelijk voor een gedegen privacy- en informatiebeveiligingsbeleid en dienen voor de eigen organisatie de noodzakelijke maatregelen te treffen om privacy en informatiebeveiliging te borgen. De volgende handleidingen kunnen daarbij handvatten bieden:

* [Handleiding Algemene verordening gegevensbescherming (AVG)](https://www.rijksoverheid.nl/documenten/rapporten/2018/01/22/handleiding-algemene-verordening-gegevensbescherming). Uitgegeven door het Ministerie van Justitie en Veiligheid.
* [Guidelines on Data Protection Impact Assessment (DPIA) (wp248rev.01)](https://ec.europa.eu/newsroom/article29/item-detail.cfm?item_id=611236). Van de voorganger van de EDPB, waarvan ook een Nederlandstalige versie beschikbaar is.

Aanbieders en afnemers wordt aanbevolen om gegevens verzameld in het kader van KIK-V in rust te versleutelen. Zo kunnen ongeautoriseerde toegang en wijzigingen van gegevens worden voorkomen.
In aanvulling daarop wordt aanbieders aanbevolen om 1) periodiek te toetsen of de aggregatie van persoonsgegevens daadwerkelijk leidt tot onomkeerbare anonimisering en 2) een infrastructuur te gebruiken die binnen de Europese Economische ruimte valt bij het verwerken van persoonsgegevens voor de aggregatie. 

## Herleidbaarheid persoonsgegevens
In het kader van gegevensuitwisseling binnen KIK-V verband is het principe dat er geen persoonsgegevens, of tot de persoon herleidbare gegevens, worden uitgewisseld. Gegevens zijn niet van toepassing op personen of zijn voldoende geabstraheerd en geanonimiseerd. In die gevallen wordt ook gecontroleerd of de populaties niet te klein zijn om alsnog de mogelijkheid te creëren de gegevens tot een persoon te herleiden. 

Daarbij zijn een paar overwegingen te geven:
* Als voor het herleiden een extra berekening (en dus een extra verwerking) moet worden uitgevoerd met gegevens waarover alleen de vragende ketenpartij kan beschikken, terwijl de organisatie wel een wettelijke grondslag voor de verwerking van de persoonsgegevens heeft, dan wordt in overleg met de keten(partij) bepaald of een uitzondering op het principe wordt gemaakt. De impact van dit herleiden is klein omdat de ketenpartijen zich aan vertrouwelijkheid moeten houden en vaak langs andere weg deze informatie ook al bezitten.
* Als het weglaten van bijvoorbeeld het totaalcijfer of  een deel van de gegevens een grote impact heeft op de bruikbaarheid van de gegevens, terwijl de organisatie wel een wettelijke grondslag heeft, dan wordt in overleg met de keten(partij) bepaald of een uitzondering op het principe wordt gemaakt.

Daarom is het beleid binnen de afspraken KIK-V om te steunen op de algemene professionaliteit en vertrouwelijkheid van de (medewerkers bij de) betrokken ketenpartijen. 

## Logging
Van afnemers en aanbieders wordt verwacht dat zij verwerkingen in het kader van KIK-V loggen. Zo kan tijdig worden ingegrepen bij onrechtmatige verwerkingen, hetzij als gevolg van (on)opzettelijk foutief handelen, kwaadwillende derden of beheersfouten.

Voor aanbieders volgt de grondslag om te loggen uit een combinatie van de Algemene Verordening Gegevensbescherming, de Wet aanvullende bepalingen verwerking persoonsgegevens in de zorg en het Besluit elektronische gegevensverwerking door zorgaanbieders (zie [Juridisch kader](/Documentatie/juridisch-kader/)). In deze wet- en regelgeving is tevens vastgelegd dat logging moet plaatsvinden volgens de NEN-7513 norm.

Voor afnemers bestaan verschillende grondslagen voor logging, afhankelijk per afnemer en situatie. De afnemer dient in het kader van de eigen bedrijfsvoering te bepalen op welke wijze logging wordt toegepast, wat hiervoor de grondslag is en in hoeverre daarbij persoonsgegevens van medewerkers worden verwerkt (zie [Juridisch kader](/Documentatie/juridisch-kader/)).

## Verwerking persoonsgegevens medewerkers
Bij het aggregeren van gegevens voor verstrekking binnen KIK-V (door aanbieders) en bij de randvoorwaardelijke logging (door aanbieders en afnemers) worden persoonsgegevens van medewerkers verwerkt. Aanbieders en afnemers zijn zelf verantwoordelijkheid om deze verwerkingen, waarin persoonsgegevens van het personeel worden verwerkt, ter goedkeuring voor te leggen bij de ondernemingsraad van de eigen organisatie (indien aanwezig). Dit voor zover de afnemer/aanbieder dit niet al heeft gedaan in het kader van het algemene informatiebeveiligingsbeleid. De verplichting volgt uit artikel 27 lid 1 sub k van de Wet op de ondernemingsraden (zie [Juridisch kader](/Documentatie/juridisch-kader/)).

