---
title: Publicaties modelgegevensset
weight: 3
---
Deze pagina bevat de meest actuele publicatie van de modelgegevensset behorend bij de geldende release van de afsprakenset.

De modelgegevensset is beschreven in een ONZ-ontologie:

* [Publicatie ONZ Generiek](https://purl.org/ozo/onz-g)
* [Publicatie ONZ Personeel](https://purl.org/ozo/onz-pers)
* [Publicatie ONZ Financieel](https://purl.org/ozo/onz-fin)
* [Publicatie ONZ Zorg](https://purl.org/ozo/onz-zorg)
* [Publicatie ONZ Organisatie](https://purl.org/ozo/onz-org)