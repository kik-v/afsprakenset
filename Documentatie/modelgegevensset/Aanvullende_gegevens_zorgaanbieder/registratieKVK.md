---
title: Toelichting bij keuze voor registratie en gebruik KvK organisatie- en vestigingsnummer
weight: 2
---

Waarom is binnen KIK-V gekozen voor de KvK-registratie als identificatie van een organisatie en/of vestiging?

Bij KIK-V gaat het om gegevens over de organisatie (kwaliteit, bedrijfsvoering, keuze informatie). En om in de gegevensuitwisseling tussen ‘vraagsteller’ en ‘beantwoorder’ ervoor te zorgen dat duidelijk is over welke organisatie en over welk onderdeel van de organisatie de antwoorden gaan, is een unieke sleutel nodig voor de organisatie en elk onderdeel. 

Om gegevens onderling goed vergelijkbaar te maken, is het van belang dat de sleutel voor alle organisaties op dezelfde niveaus worden gehanteerd. Die zouden er hetzelfde onder moeten verstaan. 

Er is in de zorg niet een overkoepelende sleutel die gehanteerd wordt in de gegevensuitwisseling met de bij KIK-V betrokken partijen. In verschillende informatiestromen worden andere sleutels gebruikt. Voor KIK-V is het wel van belang dat organisaties eenzelfde sleutel en bijbehorende zelfde structuur/ vergelijkbare niveaus van organisatie en organisatieonderdelen gebruiken.

In een door KIK-V geïnitieerd onderzoek in 2020 van bureau Berenschot naar de mogelijkheden, blijkt koppeling van gegevens op basis van het KvK-nummer de beste optie te zijn. De identificatie van een organisatie en een vestiging vindt in de KIK-V gegevensuitwisseling dus plaats aan de hand van de KvK-registratie. Deze optie is getoetst tijdens een werksessie met KIK-V ketenpartijen, besproken in het tactisch overleg en als aanvullend gegeven vastgesteld in de ketenraad. 

## Wat wordt bedoeld met het begrip “vestiging”?
De Kamer van Koophandel (KvK) hanteert de volgende definitie voor het begrip “vestiging: een vestiging is een gebouw of complex van gebouwen waar duurzame uitoefening van de activiteiten van een onderneming of rechtspersoon plaatsvindt (website KvK, 2025).

## Waarom en met welke voordelen heeft het gebruik van het KvK vestigingsnummer als koppelingsmogelijkheid van gegevens op vestigingsniveau?

De belangrijkste voordelen van het gebruik van het KvK-vestigingsnummer zijn:
-	 Door de wettelijke taak die de KVK heeft met het onderhouden van het handelsregister, is de definitie van een KVK-(vestigings)nummer zeer stabiel, duidelijk en publiekelijk toegankelijk.

-	 Daarnaast is er een verplichting vanuit de handelsregisterwet om organisatie en vestigingen waar economische activiteit (dus gedeclareerde zorg) plaatsvindt, te registreren. Zorgorganisatie hebben dus de plicht om hun registratie bij te houden. 

-	 Een koppeling van kwaliteits- en bedrijfsvoeringsgegevens op basis van het KvK-vestigingsnummer maakt koppeling tussen vele databases mogelijk op vestigingsniveau en versimpelt de koppeling met andere databases op concernniveau, als zijnde dat vestigingen weer opgeteld kunnen worden tot concernniveau. 

-	  Het KvK-vestigingsnummer wordt op vestigingsniveau gebruikt voor registraties in de databases van de AGB, ODB en KvK. Het KvK-vestigingsnummer zou dus gebruikt kunnen worden om een directe koppeling te leggen tussen entiteiten in deze drie databases, wanneer deze is geregistreerd in deze drie databases. Bovendien wordt het KvK (vestigings)nummer ook in gegevensuitwisseling buiten het zorgdomein gehanteerd en is daarmee een belangrijke sleutel in andere context.

-	 Het KvK-vestigingsnummer is makkelijk te herleiden aan het KvK-nummer dat in veel databases gebruikt wordt voor registraties op concernniveau. Dit maakt koppelingen (ook op een hoger niveau) mogelijk met de JVZ en Zorgkaart Nederland

Nadelen van het gebruik van het KvK vestigingsnummer als koppelingsmogelijkheid van gegevens op vestigingsniveau zijn:

-	Zelfstandige woonruimten waar zorg wordt geleverd, worden door de KvK alleen als vestiging gezien wanneer deze zorg 24/7 wordt geleverd, waardoor zorg bij mensen thuis niet kan worden geregistreerd als aparte vestiging.

In het onderzoek van Berenschot is in 2020 geconcludeerd: ‘Het adres van de zelfstandige bewoner achten wij teveel in de private sfeer te liggen. Hierbij gaat het eerder om de kwaliteit van de professional dan de kwaliteit van de vestiging. Het is niet te verwachten dat de definitie van het KvK-vestigingsnummer op korte termijn aan te passen is om dichter bij het gewenste locatieniveau te koppelen’.

## Waarom is niet gekozen voor AGB-code als koppelingsmogelijkheid van gegevens op vestigingsniveau?

De AGB-vestigingsnummers worden alleen aangevraagd door declarerende vestigingen en omvatten dus niet elke vestiging waar zorg wordt geleverd. Daarbij komt dat registratie in het AGB-register niet verplicht is.

## Waarom is niet gekozen voor het NZa-nummer als koppelingsmogelijkheid van gegevens op vestigingsniveau?

De database van de NZa gebruikt een eigen sleutel, de NZa-code, om in deze bron te registreren op welke locatie een specifieke behandeling wordt ondergaan. Deze code is weer lastig te koppelen omdat de code geen unieke entiteit hoeft te beschrijven. Dit komt omdat de NZa ondernemingen zelf laat kiezen op welk niveau de code gewenst is om de registratielast te verkleinen. Een NZa-code kan dus verwijzen naar één onderneming, een onderdeel van een onderneming of een groep ondernemingen.

## Wat betekent de keuze binnen het programma KIK-V voor het KvK-vestigingsnummer als koppelingsmanier van gegevens op vestigingsniveau voor mij als zorgaanbieder?

Gegevens die de organisatie registreert moeten intern gekoppeld kunnen worden aan de organisatie of aan een vestiging binnen de organisatie. Daarvoor moet:
-	 Registratie in interne systemen op orde zijn
-	 Interne registratie van deze codes beschikbaar zijn
-	 Gegevens aan die codes gekoppeld zijn/ kunnen worden
