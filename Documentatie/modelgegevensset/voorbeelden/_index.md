---
title: Voorbeeld werking modelgegevensset
weight: 1
---
In de modelgegevensset zijn verschillende concepten opgenomen die verschillende soorten informatiebehoeften kunnen dienen. Een voorbeeld hiervan is het concept rondom overeenkomsten. In de modelgegevensset zijn namelijk verschillende concepten m.b.t. overeenkomsten opgenomen:

Voorbeeld:
1. Afnemer A is geïnteresseerd in het aantal medewerkers met een arbeidsovereenkomst voor onbepaalde tijd + arbeidsovereenkomst voor bepaalde tijd + oproepovereenkomst (**exclusief stage-overeenkomst** en inhuurovereenkomst). In de grafiek hieronder is dit dus de optelsom van blauw, rood en oranje.
2. Afnemer B is geïnteresseerd in het aantal medewerkers met een arbeidsovereenkomst voor onbepaalde tijd + arbeidsovereenkomst voor bepaalde tijd + oproepovereenkomst **+ stage-overeenkomst** (exclusief oproepovereenkomst). In de grafiek hieronder is dit dus de optelsom van blauw, rood, oranje en paars.

![Voorbeeld AOVK01](voorbeeld-aovk01.png)

Een ander voorbeeld is het aantal werknemers naar kwalificatieniveau. Ook hierbij kunnen door afnemende partijen verschillende combinaties van kwalificatieniveaus gevraagd worden.

![Voorbeeld AOVK02](voorbeeld-aovk02.png)

De kracht van deze indeling van concepten is dan ook dat verschillende combinaties van concepten vanuit één gegevensset beantwoord worden.

Daarnaast is het niet nodig dat alle afnemers dezelfde definities hanteren. In het uitwisselprofiel kunnen vragen over kwaliteit en de bedrijfsvoering per afnemer verschillen  (bijvoorbeeld: de ene partij definieert een fte als 1500 uur per jaar en de andere partij als 1878 uur per jaar). In het uitwisselprofiel van de afnemer wordt aangegeven welke berekening, op basis van de modelgegevensset, wordt verwacht. Aanbieders hoeven door het gebruik van een uniforme modelgegevensset in hun systemen geen rekening te houden met deze verschillen in de vraag. De kracht van het model is dat met een vrij beperkte set veel vragen beantwoord kunnen worden. Door steeds een aantal elementen toe te voegen, kunnen steeds meer vragen worden beantwoord.

**Voorbeeld 1:** als bekend is hoeveel medewerkers met een bepaalde arbeidsovereenkomst in een organisatie werkzaam zijn en je kent het aantal gewerkte uren, kun je op basis daarvan het aantal fte berekenen.

**Voorbeeld 2:** als bekend is hoeveel medewerkers er binnen een organisatie werkzaam zijn en welk kwalificatieniveau zij hebben op een bepaald moment, kan ook de doorstroom tussen de kwalificatieniveaus worden berekend.