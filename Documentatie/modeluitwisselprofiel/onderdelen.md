---
title: Onderdelen
weight: 1
---
In een uitwisselprofiel wordt opgenomen:

### Titel
* Uit de titel van het uitwisselprofiel wordt duidelijk welke afnemer voor welk doel een uitwisseling afspreekt.
* De titel van het uitwisselprofiel begint met de naam van de afnemer, zodat in het overzicht van uitwisselprofielen snel onderscheidenlijk is voor welke uitwisseling die bedoeld is.

### Algemeen
* Metadata en release-informatie.
* Doel van de uitvraag en wat er met de gevraagde informatie wordt gedaan.
* Per informatievraag doel en rationale van de vraag.

### Juridisch
* Wat de juridische grondslag van de afnemer voor de gegevensverwerking is. Hiervoor wordt verwezen naar de relevante artikelen in het [Juridisch kader](/content/docs/juridisch-kader/)
* Wat de juridische grondslag van de aanbieder is voor de gegevensverwerking die nodig is om het antwoord samen te stellen, als het hier om het verwerken van persoonsgegevens gaat. Hiervoor wordt verwezen naar de relevante artikelen in het [Juridisch kader](/content/docs/juridisch-kader/)
* Wat het doel van de gegevensverwerking is. Op basis van het doel moet worden bepaald welke gegevens noodzakelijk en proportioneel zijn om het doel te bereiken.
* Wat een aanbieder moet doen als die niet in staat is aan te leveren conform uitwisselprofiel (m.n. via de techniek).

### Organisatorisch
* Wat de (gewenste) momenten van gegevensuitwisseling zijn, en daarmee de frequentie, zowel aanlevering als terugkoppeling. Deze momenten dienen aan te sluiten bij de [Uitwisselkalender](/content/docs/uitwisselkalender/). Hiermee wordt tevens duidelijk gemaakt wat de maximale (doorloop)tijd is voor beantwoording van een vraag vanuit een zorgaanbieder.
* Wat de (gewenste) peildata of peilperiode voor de gegevens zijn. Deze momenten dienen aan te sluiten bij de [Uitwisselkalender](/content/docs/uitwisselkalender/).
* Wat de looptijd is van het uitwisselprofiel.
* Wat de (gewenste) bewaartermijn is van de aanbiedergegevensset die door de aanbieder wordt gebruikt voor de beschreven gegevensuitwisseling.
* Welke afspraken gelden bij twijfels/vragen over de kwaliteit van data of wanneer antwoorden niet goed verwerkt kunnen worden en afspraken over een eventuele mogelijkheid tot nalevering en correctie.
* Wat de procedure is op het moment dat een aanbieder geen antwoord kan aanleveren op de vraag van de afnemer of de vraag niet goed is ontvangen door de aanbieder.
* Welke in- en exclusiecriteria gelden voor het bepalen van de doelgroep van de uitwisseling. Hieronder vallen ook eventuele drempelwaarden om te voorkomen dat geanonimiseerde gegevenssets gedeanonimiseerd kunnen worden.

### Semantisch
* Welke algemene (sleutel)informatie benodigd is over de organisatie van de aanbieder om te bepalen van wie de antwoorden komen en waarover de antwoorden gaan.
* Welke informatievragen de afnemer wil beantwoorden en welke definities daarbij gehanteerd worden (functionele beschrijving gevalideerde vragen)
  * Welke gegevenselementen nodig zijn om de informatievragen te beantwoorden. De benodigde gegevenselementen moeten onderdeel zijn van de [Modelgegevensset](/content/docs/modelgegevensset/).
  * Welke aanvullende gegevens zorgaanbieder uit het daarvoor opgenomen onderdeel in de Afsprakenset nodig is voor de uitvoering van het uitwisselprofiel.
  * Welke berekening/rekenregels van toepassing op de gegevens uit de model gegevensset KIK-V en waar deze berekening/rekenregels is gepubliceerd.
  * Welke verdeelsleutel(s) eventueel toegepast kan (kunnen) worden.
  * Wat de algemene uitgangspunten zijn die bij de vragen worden aangehouden/ van toepassing zijn.
* Welke aanvullende in- en exclusiecriteria of uitgangspunten gelden voor de informatievragen, zoals voor bepaalde selecties (en aggregatieniveau).
* Welke eisen worden gesteld aan de actualiteit, betrouwbaarheid en volledigheid van de data. Aan welke elementen je de kwaliteit van het antwoord als afnemer toetst en welke marges zijn toegestaan.
* Welke termijnen worden gesteld aan de historie van de data bij de zorgaanbieder. Tot hoe ver in het verleden moet de aanbieder de data (kunnen) aanbieden?
* Welke contextinformatie meegegeven kan worden vanuit welke bestaande kwalitatieve bron(nen).

### Technisch
* De te hanteren SPARQL queries voor de gevalideerde vragen.
* Welke manier(en) van gegevensuitwisseling wordt/worden gehanteerd voor de aanlevering door aanbieders. Uitwisseling via datastations verloopt conform [Afspraken uitwisseling via datastations](/content/docs/afspraken_gegevensuitwisseling_datastations/)
* Eisen over welk bewijs een afnemer moet beschikken om de vraag te mogen stellen.
* Welke manier(en) van gegevensuitwisseling wordt/worden gehanteerd voor de terugkoppeling door de afnemer.
* Afspraken behorend bij de implementatie van de manier(en) van gegevensuitwisseling (bijvoorbeeld over testmogelijkheden)

Afspraken over de werking van de voorzieningen zijn over het algemeen onderdeel van de documentatie van de gebruikte voorzieningen voor gegevensuitwisseling zelf. Het uitwisselprofiel verwijst naar de relevante documentatie.

### Privacy- en informatiebeveiliging
* Indien voor de aggregatie bij aanbieders gezondheidsgegevens moeten worden verwerkt, welke toetsing heeft plaatsgevonden om vast te stellen of de verwerking noodzakelijk en proportioneel is omwille van de kwaliteitsborging van zorg. Enkel in dat geval zijn de uitzonderingsgronden in AVG artikel 9, lid 1, onder h en i van toepassing (zie [Juridisch kader](/content/docs/juridisch-kader/)). Dit wordt opgenomen bij Achtergrond.
* Welke toetsing heeft plaatsgevonden om vast te stellen of met de voorgestelde aggregatie door aanbieders tot onomkeerbare anonimisering kan worden gekomen (zie ook [Toetsing privacy- en informatiebeveiliging](/content/docs/beheer_en_onderhoud/privacy_en_informatiebeveiliging/)). Hierbij is ook rekening gehouden met de effecten van het frequent uitvragen van antwoorden en het voorkomen dat inzichten in wijzigingen tussen de aangeleverde antwoorden terug te herleiden zijn naar privacygevoelige informatie. Dit wordt opgenomen bij Achtergrond.
* Welke maatregelen nodig zijn om privacy en informatiebeveiliging te borgen bij de gegevensuitwisseling. Dit is mede afhankelijk van de mate van privacy- en concurrentiegevoeligheid van gegevens. Hieronder vallen ook maatregelen die nodig zijn om te komen tot onomkeerbare anonimisering.
* Welke aanvullende maatregelen nodig zijn om concurrentiegevoelige gegevens te beschermen.

### Achtergrond
* Een toelichting bij het uitwisselprofiel. Hierin wordt aangegeven welke keuzes er zijn gemaakt in de vraagstelling of het antwoord, dan wel in het gebruik van de concepten in de modelgegevensset. Op die manier is duidelijk waarop de uitwerking in het uitwisselprofiel is gebaseerd. Deze informatie dient als achtergrond.
* Een afspraak over het (standaard) moment waarop het uitwisselprofiel wordt geëvalueerd.