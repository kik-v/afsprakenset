---
title: Modeluitwisselprofiel KIK-V
weight: 7
---
Het modeluitwisselprofiel beschrijft de [principes](/content/docs/grondslagen/principes/) voor uitwisselprofielen en de [onderdelen](/content/docs/modeluitwisselprofiel/onderdelen.md) waar ze uit bestaan.

![KIK-V Matching proces 6](kik-v-matching-proces-6.png)