---
title: Release- en versiebeschrijving
weight: 2
---
In deze sectie is meta-informatie opgenomen over de release van de afsprakenset. De Release- en versiebeschrijving duidt de positionering en status van deze publicatie. Wijzigingen ten opzichte van eerder gepubliceerde versies (en een historisch overzicht van wijzigingen) zijn opgesomd in de Changelog.
