---
title: Beschrijving
weight: 1
---
| Release | 3.1 |
|:-|:-|
| Versie | 3.1 versie 0.9|
| Doel | Release 3.0 betreft de negende publicatie van de afsprakenset en bevat wijzigingen benodigd bij of als basis voor de implementatie van nieuwe uitwisselprofielen |
| Doelgroep | (A) Ketenpartijen KIK-V; (B) Zorgaanbieders verpleeghuiszorg; (C) andere geïnteresseerden in de KIK-V Afsprakenset|
| Totstandkoming | De ontwikkeling van release 3.1 is uitgevoerd onder leiding van het project Afspraken binnen het programma KIK-V, in samenwerking met de belanghebbende partijen. Release 3.1 wordt vastgesteld door de Ketenraad KIK-V op basis van versie 0.9 |
| Inwerkingtreding | Na besluit Ketenraad |
| Operationeel toepassingsgebied | De afsprakenset release 3.1 dient als basis voor implementatie van nieuwe uitwisselprofielen die samen met deze release en daarna worden vastgesteld. |
| Functionele scope | Release 3.1 omvat hetgeen gepubliceerd in release 3.0, aangevuld met: <ul class="asterisk"><li>uitbreidingen van de model gegevensset voor de ondersteuning van nieuwe uitwisselprofielen, met name op de onderwerpen financiële gegevens en cliënt(keuze)informatie.</li><li> toevoeging van nieuwe openbare bronnen die gebruikt kunnen worden in het beantwoorden van informatiebehoeften van ketenpartijen</li> <li>toevoeging van een toelichting over het gebruik van het KvK organisatie- en KvK vestigingsnummer als aanvullende registratie en sleutel voor de herkenning van zorgaanbieders.</li></ul>|
| Licentie | [Creative Commons: Naamsvermelding-GeenAfgeleideWerken 4.0 Internationaal (CC BY-ND 4.0)](https://creativecommons.org/licenses/by-nd/4.0/).
