---
title: Afspraken gegevensuitwisseling datastations
weight: 9
---
De wijze waarop afnemers en aanbieders het KIK-V vraag- en antwoordspel vormgeven, is (mede) afhankelijk van de mate van digitalisering van de betrokken actoren. De afspraken en specificaties in dit hoofdstuk beschrijven het vraag- en antwoordspel met gebruik van datastations door zorgaanbieders en de KIK-starter door afnemers. Op basis van de afspraken en specificaties kunnen aanbieders een datastation implementeren voor toepassing van KIK-V en de uitwisseling met de KIK-starter voor afnemers. Afnemers kunnen naar de afspraken en specificaties verwijzen in de uitwisselprofielen. 

De afspraken over de gegevensuitwisseling tussen KIK-starter en datastations zijn als volgt opgebouwd:
1. Functionele beschrijving vraag- en antwoordspel
2. Gehanteerde principes
3. Benodigde verifieerbare informatie voor een decentraal KIK-V proces
4. Technische implementatie
5. Beheerafspraken gegevensuitwisseling datastations

Het laatste onderwerp wordt in overleg met betrokkenen de komende tijd verder uitgewerkt.

## Functionele beschrijving vraag- en antwoordspel

De technische afspraken over gegevensuitwisseling met datastations ondersteunen het volgende vraag- en antwoordspel:

1. Afnemer bepaalt (op basis van gehele populatie aanbieders) aan welke aanbieders zij een gevalideerde vraag wilt stellen;
2. Afnemer bepaalt (per individuele aanbieder) hoe een aanbieder (technisch) te benaderen (de afnemer wordt in deze stap ontzorgd door de KIK-starter);
3. Afnemer stelt de gevalideerde vraag aan aanbieder via de KIK-starter;
4. Aanbieder ontvangt de gevalideerde vraag en controleert of de afnemer de vraag mag stellen;
5. Aanbieder stelt het antwoord (geautomatiseerd) samen, controleert het antwoord en geeft het antwoord vrij;
6. Aanbieder stuurt het antwoord retour naar afnemer via de KIK-starter;
7. Afnemer ontvangt het antwoord in de KIK-starter en bevestigt ontvangst aan aanbieder.

![Functionele beschrijving vraag en antwoordspel](functionele_beschrijving_vraag_en_antwoordspel.png)

Hierboven en op de onderliggende pagina’s wordt beschreven hoe dit functionele vraag- en antwoordspel zich vertaalt in een technische uitwerking.

## Gehanteerde principes

Voor de gegevensuitwisseling tussen KIK-starter en datastations zijn de algemene principes van de Afsprakenset KIK-V van toepassing. Hieruit volgen zaken zoals de toepassing van de Principes Informatiestelsel voor de zorg (P12) en de FAIR-dataprincipes (P10).

Daarbovenop zijn de afspraken opgebouwd aan de hand van de volgende drie principes:

1. Schaalbaar decentraal model;
2. Zorginfrastructuur-onafhankelijk;
3. Correcte implementatie is eigen verantwoordelijkheid afnemer/aanbieder.

Deze principes worden hieronder nader toegelicht.

### Principe 1. Schaalbaar decentraal model

De potentie van de KIK-V werkwijze voor het meervoudig gebruik van data is groot. Doordat gegevens, inclusief de onderlinge samenhang tussen/betekenis van deze gegevens, gestandaardiseerd in datastations van aanbieders worden vastgelegd, is het mogelijk om met dezelfde gegevens te voorzien in antwoorden op een groot palet aan vragen.
De technische uitwisseling met datastations dient te voorzien in dezelfde potentie aan schaalbaarheid. Om die reden is ervoor gekozen om het vraag- en antwoordspel zo in te richten dat zowel afnemers en zorgaanbieders in soort en aantal gemakkelijk deel kunnen nemen aan deze uitwisseling. 

Voor het genereren van verifieerbare cryptografische bewijzen wordt de W3C-standaard Verifiable Credentials toegepast. Deze standaard kent drie rollen, namelijk die van _issuer_ (uitgever), _holder_ (houder) en _verifier_(verificateur). De _issuer_ is een derde partij die autoriteit is over bepaalde informatie. Deze geeft een verklaring uit in de vorm van een verifieerbaar cryptografisch bewijs. De _holder_ ontvangt dit verifieerbare bewijs en is de partij waarover de verklaring gaat. De _holder_ past het technische bewijs toe in het rechtstreekse, decentrale proces met de _verifier_. Om de informatie in het bewijs te kunnen vertrouwen, controleert de _verifier_ de waarachtigheid van de verklaring van de _issuer_. Dit doet de _verifier_ tegen een onderliggende gezamenlijke vertrouwensregistratie (verifiable data registry). _Issuer_, _holder_ en _verifier_ zijn allen technisch aangesloten op deze _verifiable data registry_.

Zie voor de onderlinge verhoudingen tussen de rollen ook onderstaande afbeelding.

![Principe 1](principe_1.png)

_Figuur 1. W3C-standaard Verifiable Credentials_

Zie Benodigde verifieerbare informatie voor een decentraal KIK-V proces voor meer informatie over de Verifiable Credentials die in de gegevensuitwisseling tussen KIK-starter en datastations van aanbieders worden ingezet. 

### Principe 2. Zorginfrastructuur-onafhankelijk

KIK-V realiseert en beheert geen zorginfrastructuur en maakt hergebruik van veldafspraken hierover. Het KIK-V vraag- en antwoordspel via datastations met de KIK-starter en met gebruik van de standaard Verifiable Credentials moet in principe toe te passen zijn via elke onderliggende zorginfrastructuur.

**Voorlopige afbakening zorginfrastructuren**

Het streven om zorginfrastructuur-onafhankelijk te werken, levert op korte termijn een aantal praktische issues op. In het geval dat (individuele) afnemers en aanbieders afwijkende keuzes maken voor de infrastructuur waarop zij hun koppelvlak baseren, dan komt dit samen als een interoperabiliteitsissue binnen KIK-V. Koppelvlakken uit verschillende infrastructuren begrijpen elkaar namelijk niet zondermeer.
Alhoewel KIK-V zorginfrastructuur-onafhankelijk werkt, is voor de korte termijn een afbakening nodig in de toepassing van zorginfrastructuren. Dit om te voorkomen dat interoperabiliteitsissues binnen KIK-V samenkomen.
De KIK-V ketenpartijen hebben aan het programma gevraagd om deze afbakening als volgt vorm te geven:
1. Door de implementatie van het KIK-V vraag- en antwoordspel met behulp van de KIK-starter via Nuts uit te werken in specificaties;
2. Door toe te werken naar interoperabiliteitsafspraken tussen Nuts en nID om te komen tot de voor alle partijen meest kostenefficiënte oplossing (nID wordt binnen iWLZ door zorgkantoren toegepast als zorginfrastructuur).

In 2023 is het voornemen geweest om vanuit deze afbakening beide soorten afspraken te beproeven in de pilot gegevensuitwisseling datastations. De pilot heeft uiteindelijk echter niet geleid tot de implementatie van interoperabiliteitsafspraken tussen Nuts en nID. Begin 2023 heeft de Werkgroep Interoperabiliteit Nuts-nID namelijk geadviseerd om op korte termijn niet toe te werken naar interoperabiliteit tussen Nuts en nID. In plaats daarvan is geadviseerd om vanuit Nuts en nID voorlopig parallel aan elkaar toe te werken naar een gedeeld toekomstbeeld o.b.v. soortgelijke standaarden (o.a. Verifiable Credentials, DID’s) en principes (o.a. DIZRA). Eind 2023 lijkt de werkgroep toch een (informele) doorstart te krijgen en wordt tussen Nuts en nID/iWLZ doorgewerkt aan een interoperabliteitsoplossing. Deze oplossing is gebaseerd op een beperkt aantal standaarden en voorziet vooralsnog niet in alle zaken die nodig zijn voor het KIK-V vraag en antwoordspel. Zo is het werken met verifiable credentials bijvoorbeeld nog geen onderdeel van de scope. KIK-V heeft daarom met iWLZ afgesproken om vanaf de zijlijn betrokken te blijven bij de ontwikkelingen en, zodra oplossingen via Nuts beschikbaar komen, te onderzoeken wat de impact is op de huidige Nuts specificaties. In de tussentijd past het programma de met succes in de pilot beproefde Nuts specificaties toe voor gegevensuitwisseling tussen de KIK-starter voor afnemers en de datastations voor aanbieders. 

### Principe 3. Correcte implementatie is eigen verantwoordelijkheid afnemer/aanbieder

KIK-V bouwt, net als andere use cases voor gegevensuitwisseling in de zorg, zoveel mogelijk voort op (landelijke) afspraken op het gebied van zorginfrastructuur en informatiebeveiliging. De overtuiging vanuit KIK-V is dat het steeds opnieuw vormgeven van toetsing op deze afspraken binnen de verschillende initiatieven niet de meest schaalbare oplossing in Nederland vormt. KIK-V voorziet daarom niet in aparte toetsing van correcte implementatie van KIK-V overstijgende afspraken (NEN7510/7512/7513, koppelvlakken van zorginfrastructuren, etc.). Waar daar een bredere behoefte voor is, zou toetsing van een correcte implementatie van deze bredere afspraken KIK-V overstijgend kunnen worden vormgegeven. Voor zover het de implementatie van KIK-V betreft, wordt de correcte implementatie van deze afspraken als de eigen verantwoordelijkheid van afnemers en aanbieders gezien. De Beheerorganisatie KIK-V draagt die verantwoordelijkheid voor de KIK-starter KIK-V en het credentialsplatform.

De beheerorganisatie KIK-V voorziet overigens wel in een meldpunt voor problemen bij de gegevensuitwisseling. Dit meldpunt heeft met name als functie om patronen in meldingen te identificeren en deze neer te leggen bij de relevante probleemeigenaar.

### Implementatie vraag- en antwoordspel via Nuts
De volgende (technische) documentatie beschrijft het KIK-V vraag- en antwoordspel via Nuts. Deze documentatie bestaat uit een generiek Nuts-deel en een leveranciersspecificatie specifiek voor KIK-V, de Nuts bolt. Zie onderstaande tabel voor de verwijzingen naar de betreffende documentatie:
1. Nuts standaarden en referentie-implementatie Nuts-node (beheerd door Nuts): Nuts documentatie https://nuts.nl/documentatie/ 
2. Technische uitwerking vraag- en antwoordspel KIK-V via Nuts (Nuts toepassing voor KIK-V, beheerd door KIK-V): Nuts toepassing door KIK-V https://kik-v-publicatieplatform.nl/documentatie/nuts-technischespecificaties/

In aanvulling op de technische documentatie is door Nuts en KIK-V gezamenlijk ook een aantal samenwerkingsafspraken (https://drive.google.com/drive/folders/1tURKpSiao56lmAeQHAr-BzXL-9tbheaF?usp=sharing) opgesteld. Deze richten zich op het borgen van een goede afstemming tussen twee (nog redelijk jonge) initiatieven. De samenwerkingsafspraken worden in gezamenlijkheid door de twee initiatieven beheerd.
