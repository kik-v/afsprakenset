---
title: Benodigde verifieerbare informatie gegevensuitwisseling datastations
weight: 1
---
Afnemers en aanbieders moeten kunnen terugvallen op zekere “verifieerbare informatie” om het decentrale KIK-V vraag- en antwoordspel mogelijk te maken (zoals ook toegelicht bij principe 1). Het gaat om de volgende zaken:

* De identiteit van een aanbieder achter diens (systeem)koppelvlak;
* De identiteit van een afnemer achter diens (systeem)koppelvlak;
* Dat een zekere afnemer een zekere vraag mag stellen aan de aanbieder.

Idealiter volgt de verifieerbare informatie over de eerste twee zaken uit authentieke bronnen van landelijke autoriteiten. Informatie over het laatste punt, welke afnemer welke vraag mag stellen, volgt logischerwijs na vaststelling van een uitwisselprofiel binnen de governance van KIK-V. Deze informatie kan dus het beste door de beheerorganisatie KIK-V beschikbaar kunnen worden gesteld voor gebruik bij de gegevensuitwisseling.

**Ontsluiting landelijke authentieke bronnen en work-arounds**

Informatie uit landelijke bronnen is op dit moment nog niet te ontsluiten via zorginfrastructuren en de standaard Verifiable Credentials. De wens om afspraken te maken met landelijke autoriteiten en hun informatie middels Verifiable Credentials toegankelijk te maken voor gebruik in de Nederlandse zorg, ligt bij VWS. KIK-V blijft de komende tijd betrokken bij de afstemming hierover.

Bij het gebrek aan landelijke ontsluiting van authentieke bronnen, maakt KIK-V in de eerstvolgende implementatiefase gebruik van een aantal work-arounds. Samen met de beheerorganisatie wordt de komende tijd verder uitgewerkt hoe zij tijdelijk kan voorzien in credentials met informatie over de identiteiten van aanbieders en afnemers.

Samengevat komt daarmee alle benodigde verifieerbare informatie in de eerstvolgende implementatiefase van de beheerorganisatie KIK-V.

In beheerafspraken gegevensuitwisseling datastations wordt nader uitgewerkt hoe de benodigde verifieerbare informatie in de vorm van credentials bij de beheerorganisatie KIK-V kan worden verkregen.