---
title: Beheerafspraken gegevensuitwisseling datastations
weight: 2
---

De beheerafspraken gegevensuitwisseling datastations beschrijven hoe afnemers en aanbieders de benodigde verifieerbare informatie voor het decentrale KIK-V vraag- en antwoordspel kunnen verkrijgen bij de beheerorganisatie KIK-V.

### Proces 1. Uitgifte credentials gevalideerde vragen
Verantwoordelijkheid passend bij rol als autoriteit van beheerorganisatie KIK-V (geen work-around). 
* Proces wordt geïnitieerd door beheerorganisatie KIK-V na vaststelling van een uitwisselprofiel;
* Informatie voor het credential volgt uit het uitwisselprofiel;
* Voorafgaand aan uitgifte stelt de beheerorganisatie eenmalig de relatie tussen de identiteit van de afnemer en diens unieke identifier binnen Nuts (DID) vast voor gebruik binnen de KIK-starter. Hier dient de beheerorganisatie een proces voor te hebben (zie tijdelijk subproces 1.1: vaststellen identiteit afnemer);
* Beheerorganisatie KIK-V geeft credential met de gevalideerde vragen uit via eigen applicatie en Nuts-node, conform specificaties zoals beschreven in de Nuts bolt.

### Subproces 1.1. Vaststellen identiteit afnemer
* Work-around bij het gebrek aan een landelijke autoriteit voor de identiteit van afnemers die verifiable credentials kan uitgeven voor gebruik via Nuts.
* Proces geïnitieerd door (koplopende) afnemer;
* Afnemer meldt zich bij beheerorganisatie KIK-V om eigen identiteit en relatie met diens unieke identifier binnen Nuts (DID) vast te leggen voor gebruik binnen de KIK-starter. Hiervoor heeft de beheerorganisatie KIK-V een proces;
* Beheerorganisatie KIK-V legt benodigde gegevens vast over de identiteit van de afnemer en de relatie met diens unieke identifier binnen Nuts (DID) voor gebruik binnen de KIK-starter;
* De beheerorganisatie KIK-V gebruikt deze informatie voor uitgifte van de credentials met de gevalideerde vragen.

### Proces 2. Uitgifte credentials uittreksels zorgaanbiedergegevens
* Work-around bij het gebrek aan een landelijke autoriteit voor de identiteit van aanbieders die verifiable credentials kan uitgeven voor gebruik via Nuts.
* Proces geïnitieerd door (koplopende) aanbieder;
* Aanbieder meldt zich bij beheerorganisatie KIK-V om eigen identiteit en relatie met diens unieke identifier binnen Nuts (DID) te laten vaststellen. Hiervoor heeft de beheerorganisatie KIK-V een proces;
* Beheerorganisatie KIK-V legt benodigde gegevens vast over de identiteit van de aanbieder en de unieke identifier binnen Nuts (DID);
* De beheerorganisatie KIK-V gebruikt deze informatie voor uitgifte van het credential “Uittreksel zorgaanbiedergegevens”;
* Beheerorganisatie geeft credential “Uittreksel zorgaanbiedergegevens” uit via eigen applicatie en Nuts-node, conform specificaties zoals beschreven in de Nuts bolt.