---
title: Algemeen
weight: 1
---
De sectie Algemeen bestaat uit:

* [Aansprakelijkheid](/content/docs/juridisch-kader/algemeen/aansprakelijkheid.md)
* [Algemene verordening gegevensbescherming (AVG)](/content/docs/juridisch-kader/algemeen/avg.md)
* [Toezicht en controle op de naleving](/content/docs/juridisch-kader/algemeen/toezicht.md)
* [Uitvoeringswet Algemene verordening gegevensbescherming (UW-AVG)](/content/docs/juridisch-kader/algemeen/uw-avg.md)
* [Wet op de ondernemingsraden (WOR)](/content/docs/juridisch-kader/algemeen/wor.md)