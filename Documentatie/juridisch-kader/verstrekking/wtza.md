---
title: Wet toetreding zorgaanbieders (Wtza)
weight: 10
---
Geldend van 01-01-2023 t/m heden

**Relevante artikelen**

_Artikel 2_
* De zorgaanbieder die zorg als bedoeld bij of krachtens de [Wet kwaliteit, klachten en geschillen zorg](https://wetten.overheid.nl/jci1.3:c:BWBR0037173&g=2022-05-02&z=2022-05-02) wil gaan verlenen of laten verlenen, zorgt ervoor dat de verlening van die zorg niet eerder aanvangt dan nadat hij dit aan Onze Minister heeft gemeld.

* De melding geschiedt langs elektronische weg op een bij ministeriële regeling vastgestelde wijze. De daarbij door de zorgaanbieder te verstrekken gegevens kunnen per categorie van zorgaanbieders verschillen en hebben betrekking op de aard van de te verlenen zorg, de personele en materiële organisatorische inrichting en voorwaarden betreffende kwaliteit van zorg, waaronder het bepaalde in [artikel 2, tweede lid, onderdeel b, van de Wet kwaliteit, klachten en geschillen zorg](https://wetten.overheid.nl/jci1.3:c:BWBR0037173&artikel=2&g=2022-05-02&z=2022-05-02).

_Artikel 8_
* Onze Minister verstrekt aan de Wlz-uitvoerders als bedoeld in de [Wet langdurige zorg](https://wetten.overheid.nl/jci1.3:c:BWBR0035917&g=2022-05-02&z=2022-05-02) respectievelijk de zorgverzekeraars als bedoeld in de [Zorgverzekeringswet](https://wetten.overheid.nl/jci1.3:c:BWBR0018450&g=2022-05-02&z=2022-05-02) met het oog op de vervulling van hun bij of krachtens de Wet langdurige zorg of de Zorgverzekeringswet geregelde taken respectievelijk de uit hun zorgverzekeringen voortvloeiende verplichtingen het nummer van inschrijving bij de Kamer van Koophandel van de zorgaanbieder die een melding als bedoeld in [artikel 2, eerste lid](https://wetten.overheid.nl/BWBR0043797/2022-01-01#Hoofdstuk2_Paragraaf1_Artikel2), heeft gedaan.
* Het eerste lid is van overeenkomstige toepassing op zorgaanbieders aan wie een toelatingsvergunning is verleend of van wie de toelatingsvergunning wordt ingetrokken.

_Artikel 9_
Onze Minister kan persoonsgegevens, waaronder persoonsgegevens van strafrechtelijke aard als bedoeld in [paragraaf 3.2 van de Uitvoeringswet Algemene verordening gegevensbescherming](https://wetten.overheid.nl/jci1.3:c:BWBR0040940&paragraaf=3.2&g=2022-05-02&z=2022-05-02) verwerken voor zover die verwerking noodzakelijk is voor de weigering dan wel de intrekking van de toelatingsvergunning.

_Artikel 10_
De Inspectie gezondheidszorg en jeugd en de Nederlandse zorgautoriteit verstrekken Onze Minister uit eigen beweging of desgevraagd alle gegevens, waaronder persoonsgegevens als bedoeld in [artikel 9](https://wetten.overheid.nl/BWBR0043797/2022-01-01#Hoofdstuk2_Paragraaf4_Artikel9), die voor Onze Minister van belang zijn voor de weigering dan wel intrekking van een toelatingsvergunning.

**Toepassing voor de afspraken**
* Artikel 2 in de Wtza geeft aan welke zorgaanbieders gegevens moeten aanleveren als ze zorg willen gaan leveren en op welke wijze dat moet gebeuren.
* Artikelen 8 9 en 10 in de Wtza geven aan welke gegevens het ministerie mag verstrekken en zelf mag gebruiken. Daarnaast geeft het duiding aan welke gegevens de IGJ en NZa aan het ministerie mogelijk moet aanleveren.
