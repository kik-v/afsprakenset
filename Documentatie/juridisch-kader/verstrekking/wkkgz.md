---
title: Wet kwaliteit, klachten en geschillen zorg (WKKGZ)
weight: 6
---
Geldend van 01-07-2023 t/m heden

*Bron:* [WKKGZ](https://wetten.overheid.nl/BWBR0037173/2023-07-01/0)

**Relevante artikelen**

_Artikel 11g_
1. Het Zorginstituut stelt vast voor welke vormen van zorg een kwaliteitsstandaard of een meetinstrument nodig is dan wel een overeenkomstig artikel 11a in het openbaar register opgenomen kwaliteitsstandaard of meetinstrument wijziging behoeft. Hierbij bevordert het Zorginstituut de verspreiding van goede voorbeelden op het gebied van patiëntveiligheid.
2. Het Zorginstituut stelt een tijdstip vast waarop de kwaliteitsstandaard of het meetinstrument, bedoeld in het eerste lid, moet zijn opgesteld onderscheidenlijk aangepast.
3. Indien op het in het tweede lid bedoelde tijdstip geen kwaliteitsstandaard of meetinstrument is opgesteld onderscheidenlijk aangepast, kan het Zorginstituut de Adviescommissie Kwaliteit verzoeken binnen een nader te bepalen termijn hiervoor zorg te dragen en over de aldus opgestelde onderscheidenlijk aangepaste kwaliteitsstandaard overleg te plegen met relevante tripartiete partijen.
4. Het Zorginstituut bevordert de ontwikkeling van de kwaliteit van zorg en het door zorgaanbieders aanbieden van goede zorg als bedoeld in artikel 2, tweede lid.

_Artikel 11i_
1. Het Zorginstituut draagt zorg voor het verzamelen, samenvoegen en beschikbaar maken van informatie over de kwaliteit van verleende zorg:  
   a. met het oog op het recht van de cliënt een weloverwogen keuze te kunnen maken tussen verschillende zorgaanbieders; en  
   b. ten behoeve van het toezicht door de ambtenaren van de inspectie.
2. Zorgaanbieders zijn verplicht de informatie, bedoeld in het eerste lid, te rapporteren op basis van de overeenkomstig artikel 11a in het openbaar register opgenomen meetinstrumenten.
3. Bij regeling van Onze Minister wordt de instantie aangewezen waar zorgaanbieders de in het tweede lid bedoelde informatie aanleveren.

_Artikel 11h_
1. Het Zorginstituut verwerkt de persoonsgegevens waaronder gegevens over gezondheid als bedoeld in artikel 4, onderdeel 15, van de Algemene verordening gegevensbescherming, die noodzakelijk zijn voor de uitvoering van zijn opgedragen taken, bedoeld in artikel 11g.
2. Het Zorginstituut verwerkt op grond van het eerste lid slechts persoonsgegevens indien daarop pseudonimisering als bedoeld in artikel 4, onderdeel 5, van de Algemene verordening gegevensbescherming is toegepast en vervolgens onafgebroken is gecontinueerd.
3. Artikel 21, eerste lid, tweede volzin, van de Algemene verordening gegevensbescherming, is bij de verwerking door het Zorginstituut niet van toepassing.

**Toepassing voor de afspraken**
* Artikel 11g is opgenomen omdat in het vierde lid de wettelijke taak van het Zorginstituut is opgenomen om de kwaliteit van zorg en het door zorgaanbieders aanbieden van goede zorg te bevorderen. Artikel 11i geeft een nadere invulling van die taak. In artikel 11h is verder opgenomen dat voor dit doeleinde gepseudonimiseerde gezondheidsgevens mogen worden verwerkt.
* Artikel 11i bevat de wettelijke grondslag van Zorginstituut Nederland om informatie over kwaliteit van verleende zorg te verzamelen, samenvoegen en beschikbaar te maken. Hiervoor mag het zorginstituut volgens artikel 11h en artikel 68a van de Zorgverzekeringswet geen (gepseudonimiseerde) persoonsgegevens verwerken.
* Artikel 11h regelt dat voor de wettelijke taak in artikel 11g gepseudonimiseerde persoonsgegevens mogen worden verwerkt.