---
title: Mededingingswet
weight: 5
---
Geldend van 01-01-2023 t/m heden

*Bron:* [Mededingingswet](https://wetten.overheid.nl/BWBR0008691/2023-01-01)

**Relevante artikelen**

_Artikel 6_
1. Verboden zijn overeenkomsten tussen ondernemingen, besluiten van ondernemersverenigingen en onderling afgestemde feitelijke gedragingen van ondernemingen, die ertoe strekken of ten gevolge hebben dat de mededinging op de Nederlandse markt of een deel daarvan wordt verhinderd, beperkt of vervalst.
2. De krachtens het eerste lid verboden overeenkomsten en besluiten zijn van rechtswege nietig.
3. Het eerste lid geldt niet voor overeenkomsten, besluiten en onderling afgestemde feitelijke gedragingen die bijdragen tot verbetering van de productie of van de distributie of tot bevordering van de technische of economische vooruitgang, mits een billijk aandeel in de daaruit voortvloeiende voordelen de gebruikers ten goede komt, en zonder nochtans aan de betrokken ondernemingen
   * a. beperkingen op te leggen die voor het bereiken van deze doelstellingen niet onmisbaar zijn, of
   * b. de mogelijkheid te geven, voor een wezenlijk deel van de betrokken goederen en diensten de mededinging uit te schakelen.
4. Een onderneming of ondernemersvereniging die zich op het derde lid beroept, bewijst dat aan dat lid is voldaan.

**Toepassing voor de afspraken**
* Artikel 6 stelt dat het overeenkomsten die de mededinging op de Nederlandse markt verhinderen, beperken en vervalsen verboden zijn, tenzij het de technische of economische vooruitgang bevordert. Informatie over bijvoorbeeld prijzen en cliënten mogen niet (zonder meer) tussen zorgaanbieders worden gedeeld i.v.m. de onderlinge concurrentie.