---
title: Algemene wet bestuursrecht (AWB)
weight: 1
---
Geldend van 01-08-2023 t/m heden

*Bron:* [AWB](https://wetten.overheid.nl/BWBR0005537/)

**Relevante artikelen**

_Artikel 5:16_
Een toezichthouder is bevoegd inlichtingen te vorderen.

_Artikel 5:17_
1. Een toezichthouder is bevoegd inzage te vorderen van zakelijke gegevens en bescheiden.
2. Hij is bevoegd van de gegevens en bescheiden kopieën te maken.
3. Indien het maken van kopieën niet ter plaatse kan geschieden, is hij bevoegd de gegevens en bescheiden voor dat doel voor korte tijd mee te nemen tegen een door hem af te geven schriftelijk bewijs.

_Artikel 5:20_
1. Een ieder is verplicht aan een toezichthouder binnen de door hem gestelde redelijke termijn alle medewerking te verlenen die deze redelijkerwijs kan vorderen bij de uitoefening van zijn bevoegdheden.
2. Zij die uit hoofde van ambt, beroep of wettelijk voorschrift verplicht zijn tot geheimhouding, kunnen het verlenen van medewerking weigeren, voor zover dit uit hun geheimhoudingsplicht voortvloeit.

**Toepassing voor de afspraken**
* De Algemene wet bestuursrecht definieert algemene overheidstaken, beschrijft daarbij wat toezicht inhoudt en wat de rechten en plichten van toezichthouders zijn.
* Uit de Algemene wet bestuursrecht volgt de grondslag van de Inspectie Gezondheidszorg en Jeugd om gegevens te verwerken voor haar wettelijke toezichtstaak. IGJ heeft recht op alle gegevens die nodig zijn voor het uitvoeren van deze taak.