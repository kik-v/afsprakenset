---
title: Artikel 7 Burgerlijk Wetboek Boek 7 (BW-B7-Art7)
weight: 2
---
Geldend van 01-07-2023 t/m heden

*Bron:* [BW-B7](https://wetten.overheid.nl/BWBR0005290/2023-07-01)

**Relevante artikelen**

_Artikel 7:458_
1. In afwijking van het bepaalde in [artikel 457 lid 1](https://wetten.overheid.nl/BWBR0005290/2022-04-27#Boek7_Titeldeel7_Afdeling5_Artikel457) kunnen zonder toestemming van de patiënt ten behoeve van statistiek of wetenschappelijk onderzoek op het gebied van de volksgezondheid aan een ander desgevraagd inlichtingen over de patiënt of inzage in de gegevens uit het dossier worden verstrekt indien:
   * a. het vragen van toestemming in redelijkheid niet mogelijk is en met betrekking tot de uitvoering van het onderzoek is voorzien in zodanige waarborgen, dat de persoonlijke levenssfeer van de patiënt niet onevenredig wordt geschaad, of
   * b. het vragen van toestemming, gelet op de aard en het doel van het onderzoek, in redelijkheid niet kan worden verlangd en de hulpverlener zorg heeft gedragen dat de gegevens in zodanige vorm worden verstrekt dat herleiding tot individuele natuurlijke personen redelijkerwijs wordt voorkomen.

2. Verstrekking overeenkomstig lid 1 is slechts mogelijk indien:
   * a. het onderzoek een algemeen belang dient,
   * b. het onderzoek niet zonder de desbetreffende gegevens kan worden uitgevoerd, en
   * c. voor zover de betrokken patiënt tegen een verstrekking niet uitdrukkelijk bezwaar heeft gemaakt.
3. Bij een verstrekking overeenkomstig lid 1 wordt daarvan aantekening gehouden in het dossier.

**Toepassing voor de afspraken**

Ook de WGBO (art. 7:446 - 468) regelt dat zonder toestemming van de patiënt ten behoeve van statistiek of wetenschappelijk onderzoek op het gebied van de volksgezondheid, inlichtingen over de patiënt en/of inzicht in de bescheiden mag worden verstrekt.

De WGBO gaat enkel over de behandelrelatie tussen zorgverlener en patiënt. Dit houdt in dat enkel informatie die verzameld is in het kader van de dossiervorming van de zorgverlener ten behoeve van de zorgverlening aan de patiënt onder de uitzonderingen van art. 7:458 kan vallen. Onderwerpen zoals personeelssamenstelling vallen hier niet onder. Dit is geen informatie die in de context van zorgverlening aan een specifieke patiënt verzameld wordt.

Dit artikel is eventueel relevant voor een partij als het CBS.
