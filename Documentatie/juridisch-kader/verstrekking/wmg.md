---
title: Regeling openbare jaarverantwoording WMG
weight: 8
---
**Relevante artikelen**

Alle artikelen van deze regeling zijn relevant, aangezien deze regeling specifiek gericht is op het aanleveren van de jaarverantwoording zorg voor openbaarmaking. De regeling ziet er op toe wat en in wat voor vorm er verantwoord moet worden. 
In de regeling openbare jaarverantwoording WMG staat wat een zorgaanbieder voor de jaarverantwoording over een verslagjaar bij het CIBG openbaar moet maken. Dit moet uiterlijk voor 1 juli van het jaar dat volgt op het verslagjaar.
Ter beperking van de regeldruk is in de Regeling openbare jaarverantwoording WMG zoveel mogelijk aangesloten bij wat zorgaanbieders al verplicht zijn op grond van andere administratie- en verantwoordingsverplichtingen (onder andere in het Burgerlijk Wetboek). 
De openbare jaarverantwoording bestaat uit:
a. een financiële verantwoording;
b. de op grond van ministeriële regeling bij de financiële verantwoording te voegen informatie,
en c. de op grond van ministeriële regeling te vermelden andere informatie betreffende de bedrijfsvoering van de zorgaanbieder.
Daarnaast is er een niet-openbaar gedeelte bestaande uit: een vragenlijst van de Nederlandse Zorgautoriteit (hierna: NZa) en een vragenlijst van Centraal Bureau van de Statistiek (hierna: CBS) waarvan de grondslag ligt in de CBS-wet . De gegevens uit de openbare vragenlijst zijn voor iedereen toegankelijk. De gegevens uit de niet openbare vragenlijst zijn alleen voor de betreffende organisatie toegankelijk.
Doel van de openbare jaarverantwoording is te komen tot een maatschappelijke verantwoording over de besteding van de collectieve middelen. De jaarlijkse openbare jaarverantwoording verschaft tevens informatie voor het risicogestuurd toezicht van de NZa. En door het CBS wordt de jaarlijkse verantwoording (openbare deel en aanvullende uitvraag door het CBS) gebruikt voor het samenstellen van statistieken over gezondheid en zorg
De gegevens van de jaarverantwoording dienen via een digitaal portaal (DigiMV) verantwoord te worden en zijn daarna te raadplegen via de website  voor elke geïnteresseerde. Voor de duidelijkheid: dit zijn dus de gegevens uit de openbare jaarverantwoording. Niet de gegevens uit de NZA/CBS vragenlijst.
De regeling regelt dat gegevens hiervoor moeten worden aangeleverd bij het CIBG via DigiMV, welke gegevens daarvoor moeten worden aangeleverd, met welk doel dit gebeurd en wat de afspraken zijn ten aanzien van het niet kunnen aanleveren of wanneer de aanlevering van onvoldoende kwaliteit is.

**Toepassing voor de afspraken**
De regeling vormt de grondslag voor het uitwisselprofiel NZA structurele informatieverstrekking bedrijfsvoering Wmg
