---
title: Logging
weight: 3
---
De sectie Logging bestaat uit:

* [Logging door afnemers](/content/docs/juridisch-kader/logging/afnemers.md)
* [Besluit elektronische gegevensverwerking door zorgaanbieders (BEGDZA)](/content/docs/juridisch-kader/logging/begdza.md)
* [Wet aanvullende bepalingen verwerking persoonsgegevens in de zorg (WABVPZ)](/content/docs/juridisch-kader/logging/wabvpz.md)