---
title: Juridisch kader
weight: 4
---
Het juridisch kader beschrijft relevante wet- en regelgeving voor de afsprakenset KIK-V als kaderstelling voor de te maken afspraken, evenals voor de gegevensuitwisseling op basis van uitwisselprofielen. In het kader zijn de relevante artikelen uit wet- en regelgeving overgenomen en vervolgens hoe de artikelen worden geïnterpreteerd en op welke manier ze van toepassing zijn voor de KIK-V afsprakenset of de gegevensuitwisseling tussen ketenpartij(en) en zorgaanbieders. 

Het kader is geordend in categorieën die overeenkomen met de soorten handelingen die partijen uitvoeren in de gegevensuitwisseling conform de Afsprakenset KIK-V. 

* Aggregatie: dit betreft de (grondslagen voor de) activiteiten bij de zorgaanbieder om gegevens (model gegevensset) te verzamelen en te verwerken om te komen tot antwoord op de gevalideerde vragen, zoals die worden gevraagd in de uitwisselprofielen.

* Verstrekking: dit betreft de relevante wetsartikelen die van toepassing zijn op de verstrekking van de antwoorden door de zorgaanbieder aan de vragende (keten)partij, bijvoorbeeld de artikelen die de grondslag voor de uitvraag door desbetreffende ketenpartij(en) beschrijven.

* Logging: de relevante wetsartikelen met betrekking tot de logging van de handelingen van partijen bij de gegevensuitwisseling en de aggregatie.

* Algemeen: Voor de wet- en regelgeving die niet specifiek betrekking heeft op één van deze handelingen is een categorie ‘Algemeen’ toegevoegd. 

Rondom specifieke doelen of wettelijke taken van partijen kunnen aanvullende regels zijn over welke (persoons)gegevens verwerkt mogen worden. Het voert te ver om dit voor elk doel of elk soort gegevens toe te lichten in het kader. Op het moment dat de gegevensuitwisseling wordt uitgewerkt in een uitwisselprofiel, kan worden nagegaan of er nadere restricties/inkadering zijn voor het uitwisselen van gegevens.

Het juridisch kader pretendeert niet volledig te zijn en het blijft de verantwoordelijkheid van deelnemende partijen om te voldoen aan wet- en regelgeving. Er kunnen daarmee geen rechten aan het juridisch kader worden ontleend.