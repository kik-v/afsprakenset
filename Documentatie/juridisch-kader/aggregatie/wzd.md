---
title: Wet zorg en dwang (Wzd)
weight: 2
---
# Wet zorg en dwang (Wzd)

Geldend van 06-11-2021 t/m heden

*Bron:* [Wzd](https://wetten.overheid.nl/BWBR0037173/2020-01-01)

**Relevante artikelen**

_Artikel 17_
1. De zorgaanbieder zorgt ten behoeve van het toezicht door de inspectie voor het digitaal beschikbaar zijn van in ieder geval de volgende gegevens:
   * a. de naam van de cliënt, diens burgerservicenummer, de naam van de zorgverantwoordelijke en de naam van de Wzd-functionaris;
   * b. de vorm van de aan de betrokken cliënt verleende onvrijwillige zorg;
   * c. de noodzaak voor de onvrijwillige zorg;
   * d. een schriftelijke beslissing als bedoeld in [artikel 3, tweede lid](https://wetten.overheid.nl/BWBR0040632/2021-11-06#Hoofdstuk1_Artikel3);
   * e. het zorgplan of een schriftelijke beslissing als bedoeld in [artikel 15, eerste of vijfde lid](https://wetten.overheid.nl/BWBR0040632/2021-11-06#Hoofdstuk2_Paragraaf2.5_Artikel15), die legitimeert tot de vorm van onvrijwillige zorg;
   * f. het besluit tot opname en verblijf, de rechterlijke machtiging, of de beschikking tot inbewaringstelling, die legitimeert tot onvrijwillige opname, de voorwaardelijke rechterlijke machtiging, bedoeld in [artikel 28aa, eerste lid](https://wetten.overheid.nl/BWBR0040632/2021-11-06#Hoofdstuk3_Paragraaf2_Sub-paragraaf2.3a_Artikel28aa) of de beslissing van de strafrechter op grond van [artikel 2.3 van de Wet forensische zorg](https://wetten.overheid.nl/jci1.3:c:BWBR0040634&artikel=2.3&g=2022-01-12&z=2022-01-12) waaruit blijkt of de cliënt is opgenomen met een nog geldende justitiële titel op grond van het [Wetboek van Strafrecht](https://wetten.overheid.nl/jci1.3:c:BWBR0001854&g=2022-01-12&z=2022-01-12);
   * g. de begindatum en de einddatum van de onvrijwillige zorg;
   * h. de duur en de frequentie van de onvrijwillige zorg;
   * i. de beslissingen van de zorgaanbieder op de aanvragen voor verlof of ontslag op grond van de [artikelen 47](https://wetten.overheid.nl/BWBR0040632/2021-11-06#Hoofdstuk3_Paragraaf4_Artikel47) of [48](https://wetten.overheid.nl/BWBR0040632/2021-11-06#Hoofdstuk3_Paragraaf4_Artikel48);
   * j. de beoordelingen van de Wzd-functionaris, bedoeld in de [artikelen 11a](https://wetten.overheid.nl/BWBR0040632/2021-11-06#Hoofdstuk2_Paragraaf2.3_Artikel11a), [47, derde, achtste en negende lid](https://wetten.overheid.nl/BWBR0040632/2021-11-06#Hoofdstuk3_Paragraaf4_Artikel47), en [48, zesde en tiende lid](https://wetten.overheid.nl/BWBR0040632/2021-11-06#Hoofdstuk3_Paragraaf4_Artikel48).
2. De zorgaanbieder verstrekt ten minste eens per zes maanden aan de inspectie een digitaal overzicht van de gegevens, bedoeld in het eerste lid. Bij algemene maatregel van bestuur kan worden bepaald dat deze gegevens in plaats van aan de inspectie op een bij of krachtens die maatregel aangewezen wijze verstrekt worden aan en verwerkt worden door een door Onze Minister aan te wijzen instantie.

_Artikel 18_
1. De zorgaanbieder verstrekt ten minste eens per zes maanden aan de inspectie een door het bestuur van de zorgaanbieder ondertekende analyse over de onvrijwillige zorg die door hem in die periode is verleend.
2. Bij regeling van Onze Minister kunnen regels worden gesteld over de inhoud en de wijze van verstrekken van de analyse.

_Artikel 42_
1. De zorgaanbieder die de zorg verleent in de accommodatie waar de cliënt wordt of blijft opgenomen, doet van deze opname zo spoedig mogelijk mededeling aan:
   1. de ouders die het gezag uitoefenen;
   2. de echtgenoot, de geregistreerde partner of andere levensgezel van de cliënt, of degene door wie de cliënt wordt verzorgd;
   3. de vertegenwoordiger;
   4. de griffier van de rechtbank die de machtiging heeft verleend, en
   5. het CIZ en de inspectie

_Artikel 60a_
* Indien bij de zorgaanbieder, de Wzd-functionaris, de zorgverantwoordelijke of de zorgverlener het gegronde vermoeden bestaat dat de uitvoering van de onvrijwillige zorg ernstig tekortschiet, doet hij daarvan melding bij de inspectie.
* Indien de zorgaanbieder, de Wzd-functionaris, de zorgverantwoordelijke of de zorgverlener onvoldoende, niet of niet tijdig reageert op de klachten van de cliëntenvertrouwenspersoon over de uitvoering van de zorg, kan de cliëntenvertrouwenspersoon dit melden aan de inspectie.

**Toepassing voor de afspraken**
De inspectie houdt toezicht op de uitvoering van de Wzd. In een aantal situaties is het volgens de Wzd verplicht om de inspectie te informeren.

* Halfjaarlijkse gegevensaanlevering bij de inspectie: zorgaanbieders zijn moeten minimaal halfjaarlijks hun gegevens over de uitvoering van onvrijwillige of verplichte zorg doorgeven aan de inspectie.
* Verplichte incidentele mededelingen en overleg: een burgemeesters, college van B&W, Wlz-uitvoerder, zorgaanbieder, zorgverzekeraar of klachtencommissie moet bepaalde informatie over onvrijwillige of verplichte zorg naar de inspectie sturen.
* Meldingen of signalen van ernstige problemen: zorgaanbieders, zorgprofessionals of vertrouwenspersonen die zich zorgen maken over hoe de onvrijwillige of verplichte zorg is uitgevoerd, kunnen dat melden bij de inspectie.