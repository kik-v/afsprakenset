---
title: Aggregatie
weight: 2
---

De sectie Aggregatie bestaat uit:

* [Wet kwaliteit, klachten en geschillen zorg (WKKGZ)](/content/docs/juridisch-kader/aggregatie/wkkgz.md)
* [Wet zorg en dwang (Wzd)](/content/docs/juridisch-kader/aggregatie/wzd.md)